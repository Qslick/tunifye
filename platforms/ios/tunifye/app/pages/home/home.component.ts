import { Component, OnInit, AfterViewChecked } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "ui/page";
import * as app from "tns-core-modules/application";
import * as platform from "tns-core-modules/platform";
import { Switch } from "ui/switch";

//Plugins
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { NgSwitchCase } from "@angular/common";
// import * as Admob from "nativescript-admob";

var sound = require("nativescript-sound");

@Component({
    selector: "ns-home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.css']
})
export class HomeComponent implements AfterViewChecked {

    tabSelectedIndex: number = 0;
    loopSound: boolean = false;

    // guitarNotes: string[] = ["E", "A"];
    guitarNotes: string[] = ['E', 'A', 'D', 'G', 'B', 'E'];
    ukuleleNotes: string[] = ['G', 'C', 'E', 'A'];
    bassNotes: string[] = ['E', 'A', 'D', 'G'];

    instruments: string[] = ["Guitar", "Ukulele", "Bass"];
    imgSrc: string[] = ["~/assets/instruments/Guitar.png",
        "~/assets/instruments/Ukulele.png", "~/assets/instruments/Bass.png"];


    guitarSoundsStandard = {
        "E1": sound.create("~/sounds/guitar_notes/standard/e1.mp3"),
        "A": sound.create("~/sounds/guitar_notes/standard/a.mp3"),
        "D": sound.create("~/sounds/guitar_notes/standard/d.mp3"),
        "G": sound.create("~/sounds/guitar_notes/standard/g.mp3"),
        "B": sound.create("~/sounds/guitar_notes/standard/b.mp3"),
        "E": sound.create("~/sounds/guitar_notes/standard/e2.mp3"),
    };

   ukuleleSoundsStandard = {
        "G": sound.create("~/sounds/ukulele_notes/standard/g.mp3"),
        "C": sound.create("~/sounds/ukulele_notes/standard/c.mp3"),
        "E": sound.create("~/sounds/ukulele_notes/standard/e.mp3"),
        "A": sound.create("~/sounds/ukulele_notes/standard/a.mp3"),
    };

    bassSoundsStandard = {
        "E": sound.create("~/sounds/bass_notes/standard/e.mp3"),
        "A": sound.create("~/sounds/bass_notes/standard/a.mp3"),
        "D": sound.create("~/sounds/bass_notes/standard/d.mp3"),
        "G": sound.create("~/sounds/bass_notes/standard/g.mp3"),
    };

    public selectedIndex = 1;
    public guitarTunings: Array<string>;
    public ukuleleTunings: Array<string>;
    public bassTunings: Array<string>;

    guitarTuning: string = "Standard";
    ukuleleTuning: string = "Standard";
    bassTuning: string = "Standard";


    constructor(private routerExtensions: RouterExtensions, private page: Page) {
        this.ukuleleTunings = ["Standard Tuning: G, C, E, A", "Soprano Tuning: A4, D4, F#4, B4",
            "Concert Tuning: A4, D4, F#4, B4", "Tenor Tuning: G4 C4 E4 A4", "Concert Baritone: D3, G3, B3, E4",];
        page.actionBarHidden = true;
        page.statusBarStyle = "light";

        // this.createAdmobBanner();
        // this.createAdmobInsertion();
    }

    ngAfterViewChecked(): void {
    }

    createAdmobBanner() {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createBanner({
                // if this 'view' property is not set, the banner is overlayed on the current top most view
                // view: ..,
                testing: true, // set to false to get real banners
                size: admob.AD_SIZE.SMART_BANNER, // anything in admob.AD_SIZE, like admob.AD_SIZE.SMART_BANNER
                iosBannerId: "ca-app-pub-3940256099942544/6300978111", // add your own
                androidBannerId: "ca-app-pub-3940256099942544/6300978111", // Test ID
                // androidBannerId: "ca-app-pub-4426146470477887~9760560634", // Real ID
                // Android automatically adds the connected device as test device with testing:true, iOS does not
                iosTestDeviceIds: ["yourTestDeviceUDIDs", "canBeAddedHere"],
                margins: {
                    // if both are set, top wins
                    // top: 10,
                    bottom: 0
                }
            }).then(
                function () {
                    console.log("admob createBanner done");
                },
                function (error) {
                    console.log("admob createBanner error: " + error);
                }
            );
        }, 2000);
    }

    hideAdmobBanner() {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            // the .then(.. bit is optional btw
            admob.hideBanner().then(
                function () {
                    console.log("admob hideBanner done");
                },
                function (error) {
                    console.log("admob hideBanner error: " + error);
                }
            );
        }, 3000);

    }

    createAdmobInsertion() {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createInterstitial({
                testing: true,
                iosInterstitialId: "ca-app-pub-3940256099942544/6300978111", // add your own
                androidInterstitialId: "ca-app-pub-3940256099942544/1033173712", // Test ID
                // androidInterstitialId: "ca-app-pub-4426146470477887~9760560634", // Real ID
                // Android automatically adds the connected device as test device with testing:true, iOS does not
                // iosTestDeviceIds: ["ce97330130c9047ce0d4430d37d713b2"]
            }).then(
                function () {
                    console.log("admob createInterstitial done");
                },
                function (error) {
                    console.log("admob createInterstitial error: " + error);
                }
            );
        }, 10000);
    }

    tabChange(event): void {
        console.log("Tab " + this.tabSelectedIndex + " Selected");
    }

    guitarNoteTap(note: string) {
        console.log("Guitar Note Tapped: " + note);
        this.guitarSoundsStandard[note].reset();
        this.guitarSoundsStandard[note].play();
    }

    ukuleleNoteTap(note: string){
        console.log("Guitar Note Tapped: " + note);
        this.ukuleleSoundsStandard[note].reset();
        this.ukuleleSoundsStandard[note].play(); 
    }

    bassNoteTap(note: string) {
        console.log("Bass Note Tapped: " + note);
        this.bassSoundsStandard[note].reset();
        this.bassSoundsStandard[note].play();
    }

    settings() {
        this.routerExtensions.navigate(['settings']), {
            transition: {
                name: "slideTop",
                duration: 2000,
                curve: "linear"
            }
        };
    }

    public onchange(args: SelectedIndexChangedEventData) {
        console.log(`Drop Down selected index changed from ${args.oldIndex} to ${args.newIndex}`);
        switch (args.newIndex) {
            case 0: {
                this.ukuleleTuning = "Standard";
                break;
            }
            case 1: {
                this.ukuleleTuning = "Soprano";
                break;
            }
            case 2: {
                this.ukuleleTuning = "Concert";
                break;
            }
            case 3: {
                this.ukuleleTuning = "Tenor";
                break;
            }
            case 4: {
                this.ukuleleTuning = "Baritone";
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    }

    public onopen() {
        console.log("Drop Down opened.");
    }

    public onclose() {
        console.log("Drop Down closed.");
    }

    public loopChecked(args) {
        let firstSwitch = <Switch>args.object;
        if (firstSwitch.checked) {
            console.log("Checked")
            this.loopSound = true;
        } else {
            console.log("Un-Checked")
            this.loopSound = false;
        }
    }
}




    // ukuleleNoteTap(note: string) {
    //     console.log("Ukulele Note Tapped: " + note);
    //     this.ukuleleSoundsStandard[note].reset();
    //     this.ukuleleSoundsStandard[note].play();
    // }


    // ukuleleSoundsStandard = {
    //     "G": sound.create("~/sounds/ukulele_notes/standard/g.mp3"),
    //     "C": sound.create("~/sounds/ukulele_notes/standard/c.mp3"),
    //     "E": sound.create("~/sounds/ukulele_notes/standard/e.mp3"),
    //     "A": sound.create("~/sounds/ukulele_notes/standard/a.mp3"),
    // };