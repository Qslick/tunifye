import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "ui/page";
import * as app from "tns-core-modules/application";
import * as platform from "tns-core-modules/platform";

//Plugins
import * as elementRegistryModule from 'nativescript-angular/element-registry';
elementRegistryModule.registerElement("CardView", () => require("nativescript-cardview").CardView);

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.css']
})
export class LoginComponent implements OnInit {

    selected: number = 0;
    signInTapped: boolean = false;
    private email: string;
    private password: string;

    constructor(private routerExtensions: RouterExtensions, private page: Page) {
        page.actionBarHidden = true;
        page.statusBarStyle = "light";
    }

    ngOnInit(): void {

    }

    startTuning() {
        this.routerExtensions.navigate(['/home']);
    }

    signIn() {
        this.signInTapped = true;
    }

    signUp() {
        console.log(this.email);
    }

    back() {
        this.signInTapped = false;

    }

    googleSignin(){
        alert("Coming Soon!");
    }

    facebookSignin(){
        alert("Coming Soon!");
    }

}
