"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
// import * as Admob from "nativescript-admob";
var sound = require("nativescript-sound");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(routerExtensions, page) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.tabSelectedIndex = 0;
        this.loopSound = false;
        // guitarNotes: string[] = ["E", "A"];
        this.guitarNotes = ['E', 'A', 'D', 'G', 'B', 'E'];
        this.ukuleleNotes = ['G', 'C', 'E', 'A'];
        this.bassNotes = ['E', 'A', 'D', 'G'];
        this.instruments = ["Guitar", "Ukulele", "Bass"];
        this.imgSrc = ["~/assets/instruments/Guitar.png",
            "~/assets/instruments/Ukulele.png", "~/assets/instruments/Bass.png"];
        this.guitarSoundsStandard = {
            "E1": sound.create("~/sounds/guitar_notes/standard/e1.mp3"),
            "A": sound.create("~/sounds/guitar_notes/standard/a.mp3"),
            "D": sound.create("~/sounds/guitar_notes/standard/d.mp3"),
            "G": sound.create("~/sounds/guitar_notes/standard/g.mp3"),
            "B": sound.create("~/sounds/guitar_notes/standard/b.mp3"),
            "E": sound.create("~/sounds/guitar_notes/standard/e2.mp3"),
        };
        this.ukuleleSoundsStandard = {
            "G": sound.create("~/sounds/ukulele_notes/standard/g.mp3"),
            "C": sound.create("~/sounds/ukulele_notes/standard/c.mp3"),
            "E": sound.create("~/sounds/ukulele_notes/standard/e.mp3"),
            "A": sound.create("~/sounds/ukulele_notes/standard/a.mp3"),
        };
        this.bassSoundsStandard = {
            "E": sound.create("~/sounds/bass_notes/standard/e.mp3"),
            "A": sound.create("~/sounds/bass_notes/standard/a.mp3"),
            "D": sound.create("~/sounds/bass_notes/standard/d.mp3"),
            "G": sound.create("~/sounds/bass_notes/standard/g.mp3"),
        };
        this.selectedIndex = 1;
        this.guitarTuning = "Standard";
        this.ukuleleTuning = "Standard";
        this.bassTuning = "Standard";
        this.ukuleleTunings = ["Standard Tuning: G, C, E, A", "Soprano Tuning: A4, D4, F#4, B4",
            "Concert Tuning: A4, D4, F#4, B4", "Tenor Tuning: G4 C4 E4 A4", "Concert Baritone: D3, G3, B3, E4",];
        page.actionBarHidden = true;
        page.statusBarStyle = "light";
        // this.createAdmobBanner();
        // this.createAdmobInsertion();
    }
    HomeComponent.prototype.ngAfterViewChecked = function () {
    };
    HomeComponent.prototype.createAdmobBanner = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createBanner({
                // if this 'view' property is not set, the banner is overlayed on the current top most view
                // view: ..,
                testing: true,
                size: admob.AD_SIZE.SMART_BANNER,
                iosBannerId: "ca-app-pub-3940256099942544/6300978111",
                androidBannerId: "ca-app-pub-3940256099942544/6300978111",
                // androidBannerId: "ca-app-pub-4426146470477887~9760560634", // Real ID
                // Android automatically adds the connected device as test device with testing:true, iOS does not
                iosTestDeviceIds: ["yourTestDeviceUDIDs", "canBeAddedHere"],
                margins: {
                    // if both are set, top wins
                    // top: 10,
                    bottom: 0
                }
            }).then(function () {
                console.log("admob createBanner done");
            }, function (error) {
                console.log("admob createBanner error: " + error);
            });
        }, 2000);
    };
    HomeComponent.prototype.hideAdmobBanner = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            // the .then(.. bit is optional btw
            admob.hideBanner().then(function () {
                console.log("admob hideBanner done");
            }, function (error) {
                console.log("admob hideBanner error: " + error);
            });
        }, 3000);
    };
    HomeComponent.prototype.createAdmobInsertion = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createInterstitial({
                testing: true,
                iosInterstitialId: "ca-app-pub-3940256099942544/6300978111",
                androidInterstitialId: "ca-app-pub-3940256099942544/1033173712",
            }).then(function () {
                console.log("admob createInterstitial done");
            }, function (error) {
                console.log("admob createInterstitial error: " + error);
            });
        }, 10000);
    };
    HomeComponent.prototype.tabChange = function (event) {
        console.log("Tab " + this.tabSelectedIndex + " Selected");
    };
    // guitarNoteTap(note: string) {
    //     console.log("Guitar Note Tapped: " + note);
    //     this.guitarSoundsStandard[note].reset();
    //     this.guitarSoundsStandard[note].play();
    // }
    HomeComponent.prototype.guitarNoteTap = function (note, loop) {
        var _this = this;
        if (loop === void 0) { loop = false; }
        console.log("Guitar Note Tapped: " + note + " Loop: " + loop);
        // while (this.loopSound == false) {
        setTimeout(function () {
            console.log("Loop");
            _this.guitarSoundsStandard[note].play();
        }, 2000);
        // }
    };
    HomeComponent.prototype.ukuleleNoteTap = function (note) {
        console.log("Ukulele Note Tapped: " + note);
        this.ukuleleSoundsStandard[note].reset();
        this.ukuleleSoundsStandard[note].play();
    };
    // ukuleleNoteTap(note: string, loop: boolean = false){
    //     console.log("Ukulele Note Tapped: " + note + " Loop: " + loop);
    // }
    HomeComponent.prototype.bassNoteTap = function (note) {
        console.log("Bass Note Tapped: " + note);
        this.bassSoundsStandard[note].reset();
        this.bassSoundsStandard[note].play();
    };
    HomeComponent.prototype.settings = function () {
        this.routerExtensions.navigate(['settings']), {
            transition: {
                name: "slideTop",
                duration: 2000,
                curve: "linear"
            }
        };
    };
    HomeComponent.prototype.onchange = function (args) {
        console.log("Drop Down selected index changed from " + args.oldIndex + " to " + args.newIndex);
        switch (args.newIndex) {
            case 0: {
                this.ukuleleTuning = "Standard";
                break;
            }
            case 1: {
                this.ukuleleTuning = "Soprano";
                break;
            }
            case 2: {
                this.ukuleleTuning = "Concert";
                break;
            }
            case 3: {
                this.ukuleleTuning = "Tenor";
                break;
            }
            case 4: {
                this.ukuleleTuning = "Baritone";
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    };
    HomeComponent.prototype.onopen = function () {
        console.log("Drop Down opened.");
    };
    HomeComponent.prototype.onclose = function () {
        console.log("Drop Down closed.");
    };
    HomeComponent.prototype.loopChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            console.log("Checked");
            this.loopSound = true;
        }
        else {
            console.log("Un-Checked");
            this.loopSound = false;
        }
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "ns-home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['./home.css']
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFvRTtBQUNwRSxzREFBK0Q7QUFFL0QsZ0NBQStCO0FBUS9CLCtDQUErQztBQUUvQyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQztBQVExQztJQWdESSx1QkFBb0IsZ0JBQWtDLEVBQVUsSUFBVTtRQUF0RCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQTlDMUUscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBQzdCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFFM0Isc0NBQXNDO1FBQ3RDLGdCQUFXLEdBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZELGlCQUFZLEdBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM5QyxjQUFTLEdBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUUzQyxnQkFBVyxHQUFhLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN0RCxXQUFNLEdBQWEsQ0FBQyxpQ0FBaUM7WUFDakQsa0NBQWtDLEVBQUUsK0JBQStCLENBQUMsQ0FBQztRQUd6RSx5QkFBb0IsR0FBRztZQUNuQixJQUFJLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyx1Q0FBdUMsQ0FBQztZQUMzRCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyx1Q0FBdUMsQ0FBQztTQUM3RCxDQUFDO1FBRUYsMEJBQXFCLEdBQUc7WUFDcEIsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7WUFDMUQsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7WUFDMUQsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7WUFDMUQsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7U0FDN0QsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ2pCLEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1NBQzFELENBQUM7UUFFSyxrQkFBYSxHQUFHLENBQUMsQ0FBQztRQUt6QixpQkFBWSxHQUFXLFVBQVUsQ0FBQztRQUNsQyxrQkFBYSxHQUFXLFVBQVUsQ0FBQztRQUNuQyxlQUFVLEdBQVcsVUFBVSxDQUFDO1FBSTVCLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxpQ0FBaUM7WUFDbkYsaUNBQWlDLEVBQUUsMkJBQTJCLEVBQUUsa0NBQWtDLEVBQUUsQ0FBQztRQUN6RyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLE9BQU8sQ0FBQztRQUU5Qiw0QkFBNEI7UUFDNUIsK0JBQStCO0lBQ25DLENBQUM7SUFFRCwwQ0FBa0IsR0FBbEI7SUFDQSxDQUFDO0lBRUQseUNBQWlCLEdBQWpCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDMUMsVUFBVSxDQUFDO1lBQ1AsS0FBSyxDQUFDLFlBQVksQ0FBQztnQkFDZiwyRkFBMkY7Z0JBQzNGLFlBQVk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWTtnQkFDaEMsV0FBVyxFQUFFLHdDQUF3QztnQkFDckQsZUFBZSxFQUFFLHdDQUF3QztnQkFDekQsd0VBQXdFO2dCQUN4RSxpR0FBaUc7Z0JBQ2pHLGdCQUFnQixFQUFFLENBQUMscUJBQXFCLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQzNELE9BQU8sRUFBRTtvQkFDTCw0QkFBNEI7b0JBQzVCLFdBQVc7b0JBQ1gsTUFBTSxFQUFFLENBQUM7aUJBQ1o7YUFDSixDQUFDLENBQUMsSUFBSSxDQUNIO2dCQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQ0QsVUFBVSxLQUFLO2dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDdEQsQ0FBQyxDQUNKLENBQUM7UUFDTixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsdUNBQWUsR0FBZjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLG1DQUFtQztZQUNuQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUNuQjtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUNELFVBQVUsS0FBSztnQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBRWIsQ0FBQztJQUVELDRDQUFvQixHQUFwQjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQztnQkFDckIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsaUJBQWlCLEVBQUUsd0NBQXdDO2dCQUMzRCxxQkFBcUIsRUFBRSx3Q0FBd0M7YUFJbEUsQ0FBQyxDQUFDLElBQUksQ0FDSDtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUM7WUFDakQsQ0FBQyxFQUNELFVBQVUsS0FBSztnQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzVELENBQUMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2QsQ0FBQztJQUVELGlDQUFTLEdBQVQsVUFBVSxLQUFLO1FBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxnQ0FBZ0M7SUFDaEMsa0RBQWtEO0lBQ2xELCtDQUErQztJQUMvQyw4Q0FBOEM7SUFDOUMsSUFBSTtJQUVKLHFDQUFhLEdBQWIsVUFBYyxJQUFZLEVBQUUsSUFBcUI7UUFBakQsaUJBU0M7UUFUMkIscUJBQUEsRUFBQSxZQUFxQjtRQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLElBQUksR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDOUQsb0NBQW9DO1FBQ3BDLFVBQVUsQ0FBQztZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUk7SUFFUixDQUFDO0lBRUQsc0NBQWMsR0FBZCxVQUFlLElBQVk7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRCx1REFBdUQ7SUFDdkQsc0VBQXNFO0lBQ3RFLElBQUk7SUFFSixtQ0FBVyxHQUFYLFVBQVksSUFBWTtRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRTtZQUMxQyxVQUFVLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLEtBQUssRUFBRSxRQUFRO2FBQ2xCO1NBQ0osQ0FBQztJQUNOLENBQUM7SUFFTSxnQ0FBUSxHQUFmLFVBQWdCLElBQW1DO1FBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQXlDLElBQUksQ0FBQyxRQUFRLFlBQU8sSUFBSSxDQUFDLFFBQVUsQ0FBQyxDQUFDO1FBQzFGLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssQ0FBQyxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUM7Z0JBQ2hDLEtBQUssQ0FBQztZQUNWLENBQUM7WUFDRCxLQUFLLENBQUMsRUFBRSxDQUFDO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO2dCQUMvQixLQUFLLENBQUM7WUFDVixDQUFDO1lBQ0QsS0FBSyxDQUFDLEVBQUUsQ0FBQztnQkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztnQkFDL0IsS0FBSyxDQUFDO1lBQ1YsQ0FBQztZQUNELEtBQUssQ0FBQyxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7Z0JBQzdCLEtBQUssQ0FBQztZQUNWLENBQUM7WUFDRCxLQUFLLENBQUMsRUFBRSxDQUFDO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDO2dCQUNoQyxLQUFLLENBQUM7WUFDVixDQUFDO1lBQ0QsU0FBUyxDQUFDO2dCQUNOLGNBQWM7Z0JBQ2QsS0FBSyxDQUFDO1lBQ1YsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sOEJBQU0sR0FBYjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRU0sK0JBQU8sR0FBZDtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRU0sbUNBQVcsR0FBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDMUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMzQixDQUFDO0lBQ0wsQ0FBQztJQTdOUSxhQUFhO1FBTnpCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyxZQUFZLENBQUM7U0FDNUIsQ0FBQzt5Q0FpRHdDLHlCQUFnQixFQUFnQixXQUFJO09BaERqRSxhQUFhLENBZ096QjtJQUFELG9CQUFDO0NBQUEsQUFoT0QsSUFnT0M7QUFoT1ksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3Q2hlY2tlZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCAqIGFzIGFwcCBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvblwiO1xuaW1wb3J0ICogYXMgcGxhdGZvcm0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm1cIjtcbmltcG9ydCB7IFN3aXRjaCB9IGZyb20gXCJ1aS9zd2l0Y2hcIjtcblxuLy9QbHVnaW5zXG5pbXBvcnQgeyBTZWxlY3RlZEluZGV4Q2hhbmdlZEV2ZW50RGF0YSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duXCI7XG5pbXBvcnQgeyBOZ1N3aXRjaENhc2UgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XG4vLyBpbXBvcnQgKiBhcyBBZG1vYiBmcm9tIFwibmF0aXZlc2NyaXB0LWFkbW9iXCI7XG5cbnZhciBzb3VuZCA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtc291bmRcIik7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIm5zLWhvbWVcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vaG9tZS5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogWycuL2hvbWUuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0NoZWNrZWQge1xuXG4gICAgdGFiU2VsZWN0ZWRJbmRleDogbnVtYmVyID0gMDtcbiAgICBsb29wU291bmQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8vIGd1aXRhck5vdGVzOiBzdHJpbmdbXSA9IFtcIkVcIiwgXCJBXCJdO1xuICAgIGd1aXRhck5vdGVzOiBzdHJpbmdbXSA9IFsnRScsICdBJywgJ0QnLCAnRycsICdCJywgJ0UnXTtcbiAgICB1a3VsZWxlTm90ZXM6IHN0cmluZ1tdID0gWydHJywgJ0MnLCAnRScsICdBJ107XG4gICAgYmFzc05vdGVzOiBzdHJpbmdbXSA9IFsnRScsICdBJywgJ0QnLCAnRyddO1xuXG4gICAgaW5zdHJ1bWVudHM6IHN0cmluZ1tdID0gW1wiR3VpdGFyXCIsIFwiVWt1bGVsZVwiLCBcIkJhc3NcIl07XG4gICAgaW1nU3JjOiBzdHJpbmdbXSA9IFtcIn4vYXNzZXRzL2luc3RydW1lbnRzL0d1aXRhci5wbmdcIixcbiAgICAgICAgXCJ+L2Fzc2V0cy9pbnN0cnVtZW50cy9Va3VsZWxlLnBuZ1wiLCBcIn4vYXNzZXRzL2luc3RydW1lbnRzL0Jhc3MucG5nXCJdO1xuXG5cbiAgICBndWl0YXJTb3VuZHNTdGFuZGFyZCA9IHtcbiAgICAgICAgXCJFMVwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZTEubXAzXCIpLFxuICAgICAgICBcIkFcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvZ3VpdGFyX25vdGVzL3N0YW5kYXJkL2EubXAzXCIpLFxuICAgICAgICBcIkRcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvZ3VpdGFyX25vdGVzL3N0YW5kYXJkL2QubXAzXCIpLFxuICAgICAgICBcIkdcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvZ3VpdGFyX25vdGVzL3N0YW5kYXJkL2cubXAzXCIpLFxuICAgICAgICBcIkJcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvZ3VpdGFyX25vdGVzL3N0YW5kYXJkL2IubXAzXCIpLFxuICAgICAgICBcIkVcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvZ3VpdGFyX25vdGVzL3N0YW5kYXJkL2UyLm1wM1wiKSxcbiAgICB9O1xuXG4gICAgdWt1bGVsZVNvdW5kc1N0YW5kYXJkID0ge1xuICAgICAgICBcIkdcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9nLm1wM1wiKSxcbiAgICAgICAgXCJDXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL3VrdWxlbGVfbm90ZXMvc3RhbmRhcmQvYy5tcDNcIiksXG4gICAgICAgIFwiRVwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy91a3VsZWxlX25vdGVzL3N0YW5kYXJkL2UubXAzXCIpLFxuICAgICAgICBcIkFcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9hLm1wM1wiKSxcbiAgICB9O1xuXG4gICAgYmFzc1NvdW5kc1N0YW5kYXJkID0ge1xuICAgICAgICBcIkVcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvYmFzc19ub3Rlcy9zdGFuZGFyZC9lLm1wM1wiKSxcbiAgICAgICAgXCJBXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL2Jhc3Nfbm90ZXMvc3RhbmRhcmQvYS5tcDNcIiksXG4gICAgICAgIFwiRFwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9iYXNzX25vdGVzL3N0YW5kYXJkL2QubXAzXCIpLFxuICAgICAgICBcIkdcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvYmFzc19ub3Rlcy9zdGFuZGFyZC9nLm1wM1wiKSxcbiAgICB9O1xuXG4gICAgcHVibGljIHNlbGVjdGVkSW5kZXggPSAxO1xuICAgIHB1YmxpYyBndWl0YXJUdW5pbmdzOiBBcnJheTxzdHJpbmc+O1xuICAgIHB1YmxpYyB1a3VsZWxlVHVuaW5nczogQXJyYXk8c3RyaW5nPjtcbiAgICBwdWJsaWMgYmFzc1R1bmluZ3M6IEFycmF5PHN0cmluZz47XG5cbiAgICBndWl0YXJUdW5pbmc6IHN0cmluZyA9IFwiU3RhbmRhcmRcIjtcbiAgICB1a3VsZWxlVHVuaW5nOiBzdHJpbmcgPSBcIlN0YW5kYXJkXCI7XG4gICAgYmFzc1R1bmluZzogc3RyaW5nID0gXCJTdGFuZGFyZFwiO1xuXG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIHByaXZhdGUgcGFnZTogUGFnZSkge1xuICAgICAgICB0aGlzLnVrdWxlbGVUdW5pbmdzID0gW1wiU3RhbmRhcmQgVHVuaW5nOiBHLCBDLCBFLCBBXCIsIFwiU29wcmFubyBUdW5pbmc6IEE0LCBENCwgRiM0LCBCNFwiLFxuICAgICAgICAgICAgXCJDb25jZXJ0IFR1bmluZzogQTQsIEQ0LCBGIzQsIEI0XCIsIFwiVGVub3IgVHVuaW5nOiBHNCBDNCBFNCBBNFwiLCBcIkNvbmNlcnQgQmFyaXRvbmU6IEQzLCBHMywgQjMsIEU0XCIsXTtcbiAgICAgICAgcGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuICAgICAgICBwYWdlLnN0YXR1c0JhclN0eWxlID0gXCJsaWdodFwiO1xuXG4gICAgICAgIC8vIHRoaXMuY3JlYXRlQWRtb2JCYW5uZXIoKTtcbiAgICAgICAgLy8gdGhpcy5jcmVhdGVBZG1vYkluc2VydGlvbigpO1xuICAgIH1cblxuICAgIG5nQWZ0ZXJWaWV3Q2hlY2tlZCgpOiB2b2lkIHtcbiAgICB9XG5cbiAgICBjcmVhdGVBZG1vYkJhbm5lcigpIHtcbiAgICAgICAgdmFyIGFkbW9iID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hZG1vYlwiKTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBhZG1vYi5jcmVhdGVCYW5uZXIoe1xuICAgICAgICAgICAgICAgIC8vIGlmIHRoaXMgJ3ZpZXcnIHByb3BlcnR5IGlzIG5vdCBzZXQsIHRoZSBiYW5uZXIgaXMgb3ZlcmxheWVkIG9uIHRoZSBjdXJyZW50IHRvcCBtb3N0IHZpZXdcbiAgICAgICAgICAgICAgICAvLyB2aWV3OiAuLixcbiAgICAgICAgICAgICAgICB0ZXN0aW5nOiB0cnVlLCAvLyBzZXQgdG8gZmFsc2UgdG8gZ2V0IHJlYWwgYmFubmVyc1xuICAgICAgICAgICAgICAgIHNpemU6IGFkbW9iLkFEX1NJWkUuU01BUlRfQkFOTkVSLCAvLyBhbnl0aGluZyBpbiBhZG1vYi5BRF9TSVpFLCBsaWtlIGFkbW9iLkFEX1NJWkUuU01BUlRfQkFOTkVSXG4gICAgICAgICAgICAgICAgaW9zQmFubmVySWQ6IFwiY2EtYXBwLXB1Yi0zOTQwMjU2MDk5OTQyNTQ0LzYzMDA5NzgxMTFcIiwgLy8gYWRkIHlvdXIgb3duXG4gICAgICAgICAgICAgICAgYW5kcm9pZEJhbm5lcklkOiBcImNhLWFwcC1wdWItMzk0MDI1NjA5OTk0MjU0NC82MzAwOTc4MTExXCIsIC8vIFRlc3QgSURcbiAgICAgICAgICAgICAgICAvLyBhbmRyb2lkQmFubmVySWQ6IFwiY2EtYXBwLXB1Yi00NDI2MTQ2NDcwNDc3ODg3fjk3NjA1NjA2MzRcIiwgLy8gUmVhbCBJRFxuICAgICAgICAgICAgICAgIC8vIEFuZHJvaWQgYXV0b21hdGljYWxseSBhZGRzIHRoZSBjb25uZWN0ZWQgZGV2aWNlIGFzIHRlc3QgZGV2aWNlIHdpdGggdGVzdGluZzp0cnVlLCBpT1MgZG9lcyBub3RcbiAgICAgICAgICAgICAgICBpb3NUZXN0RGV2aWNlSWRzOiBbXCJ5b3VyVGVzdERldmljZVVESURzXCIsIFwiY2FuQmVBZGRlZEhlcmVcIl0sXG4gICAgICAgICAgICAgICAgbWFyZ2luczoge1xuICAgICAgICAgICAgICAgICAgICAvLyBpZiBib3RoIGFyZSBzZXQsIHRvcCB3aW5zXG4gICAgICAgICAgICAgICAgICAgIC8vIHRvcDogMTAsXG4gICAgICAgICAgICAgICAgICAgIGJvdHRvbTogMFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImFkbW9iIGNyZWF0ZUJhbm5lciBkb25lXCIpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgY3JlYXRlQmFubmVyIGVycm9yOiBcIiArIGVycm9yKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9LCAyMDAwKTtcbiAgICB9XG5cbiAgICBoaWRlQWRtb2JCYW5uZXIoKSB7XG4gICAgICAgIHZhciBhZG1vYiA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtYWRtb2JcIik7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgLy8gdGhlIC50aGVuKC4uIGJpdCBpcyBvcHRpb25hbCBidHdcbiAgICAgICAgICAgIGFkbW9iLmhpZGVCYW5uZXIoKS50aGVuKFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJhZG1vYiBoaWRlQmFubmVyIGRvbmVcIik7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJhZG1vYiBoaWRlQmFubmVyIGVycm9yOiBcIiArIGVycm9yKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9LCAzMDAwKTtcblxuICAgIH1cblxuICAgIGNyZWF0ZUFkbW9iSW5zZXJ0aW9uKCkge1xuICAgICAgICB2YXIgYWRtb2IgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWFkbW9iXCIpO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGFkbW9iLmNyZWF0ZUludGVyc3RpdGlhbCh7XG4gICAgICAgICAgICAgICAgdGVzdGluZzogdHJ1ZSxcbiAgICAgICAgICAgICAgICBpb3NJbnRlcnN0aXRpYWxJZDogXCJjYS1hcHAtcHViLTM5NDAyNTYwOTk5NDI1NDQvNjMwMDk3ODExMVwiLCAvLyBhZGQgeW91ciBvd25cbiAgICAgICAgICAgICAgICBhbmRyb2lkSW50ZXJzdGl0aWFsSWQ6IFwiY2EtYXBwLXB1Yi0zOTQwMjU2MDk5OTQyNTQ0LzEwMzMxNzM3MTJcIiwgLy8gVGVzdCBJRFxuICAgICAgICAgICAgICAgIC8vIGFuZHJvaWRJbnRlcnN0aXRpYWxJZDogXCJjYS1hcHAtcHViLTQ0MjYxNDY0NzA0Nzc4ODd+OTc2MDU2MDYzNFwiLCAvLyBSZWFsIElEXG4gICAgICAgICAgICAgICAgLy8gQW5kcm9pZCBhdXRvbWF0aWNhbGx5IGFkZHMgdGhlIGNvbm5lY3RlZCBkZXZpY2UgYXMgdGVzdCBkZXZpY2Ugd2l0aCB0ZXN0aW5nOnRydWUsIGlPUyBkb2VzIG5vdFxuICAgICAgICAgICAgICAgIC8vIGlvc1Rlc3REZXZpY2VJZHM6IFtcImNlOTczMzAxMzBjOTA0N2NlMGQ0NDMwZDM3ZDcxM2IyXCJdXG4gICAgICAgICAgICB9KS50aGVuKFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJhZG1vYiBjcmVhdGVJbnRlcnN0aXRpYWwgZG9uZVwiKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImFkbW9iIGNyZWF0ZUludGVyc3RpdGlhbCBlcnJvcjogXCIgKyBlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSwgMTAwMDApO1xuICAgIH1cblxuICAgIHRhYkNoYW5nZShldmVudCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyhcIlRhYiBcIiArIHRoaXMudGFiU2VsZWN0ZWRJbmRleCArIFwiIFNlbGVjdGVkXCIpO1xuICAgIH1cblxuICAgIC8vIGd1aXRhck5vdGVUYXAobm90ZTogc3RyaW5nKSB7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKFwiR3VpdGFyIE5vdGUgVGFwcGVkOiBcIiArIG5vdGUpO1xuICAgIC8vICAgICB0aGlzLmd1aXRhclNvdW5kc1N0YW5kYXJkW25vdGVdLnJlc2V0KCk7XG4gICAgLy8gICAgIHRoaXMuZ3VpdGFyU291bmRzU3RhbmRhcmRbbm90ZV0ucGxheSgpO1xuICAgIC8vIH1cblxuICAgIGd1aXRhck5vdGVUYXAobm90ZTogc3RyaW5nLCBsb29wOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJHdWl0YXIgTm90ZSBUYXBwZWQ6IFwiICsgbm90ZSArIFwiIExvb3A6IFwiICsgbG9vcCk7XG4gICAgICAgIC8vIHdoaWxlICh0aGlzLmxvb3BTb3VuZCA9PSBmYWxzZSkge1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTG9vcFwiKTtcbiAgICAgICAgICAgIHRoaXMuZ3VpdGFyU291bmRzU3RhbmRhcmRbbm90ZV0ucGxheSgpO1xuICAgICAgICB9LCAyMDAwKTtcbiAgICAgICAgLy8gfVxuXG4gICAgfVxuXG4gICAgdWt1bGVsZU5vdGVUYXAobm90ZTogc3RyaW5nKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiVWt1bGVsZSBOb3RlIFRhcHBlZDogXCIgKyBub3RlKTtcbiAgICAgICAgdGhpcy51a3VsZWxlU291bmRzU3RhbmRhcmRbbm90ZV0ucmVzZXQoKTtcbiAgICAgICAgdGhpcy51a3VsZWxlU291bmRzU3RhbmRhcmRbbm90ZV0ucGxheSgpO1xuICAgIH1cblxuICAgIC8vIHVrdWxlbGVOb3RlVGFwKG5vdGU6IHN0cmluZywgbG9vcDogYm9vbGVhbiA9IGZhbHNlKXtcbiAgICAvLyAgICAgY29uc29sZS5sb2coXCJVa3VsZWxlIE5vdGUgVGFwcGVkOiBcIiArIG5vdGUgKyBcIiBMb29wOiBcIiArIGxvb3ApO1xuICAgIC8vIH1cblxuICAgIGJhc3NOb3RlVGFwKG5vdGU6IHN0cmluZykge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkJhc3MgTm90ZSBUYXBwZWQ6IFwiICsgbm90ZSk7XG4gICAgICAgIHRoaXMuYmFzc1NvdW5kc1N0YW5kYXJkW25vdGVdLnJlc2V0KCk7XG4gICAgICAgIHRoaXMuYmFzc1NvdW5kc1N0YW5kYXJkW25vdGVdLnBsYXkoKTtcbiAgICB9XG5cbiAgICBzZXR0aW5ncygpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnc2V0dGluZ3MnXSksIHtcbiAgICAgICAgICAgIHRyYW5zaXRpb246IHtcbiAgICAgICAgICAgICAgICBuYW1lOiBcInNsaWRlVG9wXCIsXG4gICAgICAgICAgICAgICAgZHVyYXRpb246IDIwMDAsXG4gICAgICAgICAgICAgICAgY3VydmU6IFwibGluZWFyXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25jaGFuZ2UoYXJnczogU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEpIHtcbiAgICAgICAgY29uc29sZS5sb2coYERyb3AgRG93biBzZWxlY3RlZCBpbmRleCBjaGFuZ2VkIGZyb20gJHthcmdzLm9sZEluZGV4fSB0byAke2FyZ3MubmV3SW5kZXh9YCk7XG4gICAgICAgIHN3aXRjaCAoYXJncy5uZXdJbmRleCkge1xuICAgICAgICAgICAgY2FzZSAwOiB7XG4gICAgICAgICAgICAgICAgdGhpcy51a3VsZWxlVHVuaW5nID0gXCJTdGFuZGFyZFwiO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2FzZSAxOiB7XG4gICAgICAgICAgICAgICAgdGhpcy51a3VsZWxlVHVuaW5nID0gXCJTb3ByYW5vXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXNlIDI6IHtcbiAgICAgICAgICAgICAgICB0aGlzLnVrdWxlbGVUdW5pbmcgPSBcIkNvbmNlcnRcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhc2UgMzoge1xuICAgICAgICAgICAgICAgIHRoaXMudWt1bGVsZVR1bmluZyA9IFwiVGVub3JcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhc2UgNDoge1xuICAgICAgICAgICAgICAgIHRoaXMudWt1bGVsZVR1bmluZyA9IFwiQmFyaXRvbmVcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGRlZmF1bHQ6IHtcbiAgICAgICAgICAgICAgICAvL3N0YXRlbWVudHM7IFxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG9ub3BlbigpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gb3BlbmVkLlwiKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25jbG9zZSgpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJEcm9wIERvd24gY2xvc2VkLlwiKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgbG9vcENoZWNrZWQoYXJncykge1xuICAgICAgICBsZXQgZmlyc3RTd2l0Y2ggPSA8U3dpdGNoPmFyZ3Mub2JqZWN0O1xuICAgICAgICBpZiAoZmlyc3RTd2l0Y2guY2hlY2tlZCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJDaGVja2VkXCIpXG4gICAgICAgICAgICB0aGlzLmxvb3BTb3VuZCA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlVuLUNoZWNrZWRcIilcbiAgICAgICAgICAgIHRoaXMubG9vcFNvdW5kID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cblxufSJdfQ==