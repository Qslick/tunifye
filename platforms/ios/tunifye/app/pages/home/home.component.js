"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
// import * as Admob from "nativescript-admob";
var sound = require("nativescript-sound");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(routerExtensions, page) {
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.tabSelectedIndex = 0;
        this.loopSound = false;
        // guitarNotes: string[] = ["E", "A"];
        this.guitarNotes = ['E', 'A', 'D', 'G', 'B', 'E'];
        this.ukuleleNotes = ['G', 'C', 'E', 'A'];
        this.bassNotes = ['E', 'A', 'D', 'G'];
        this.instruments = ["Guitar", "Ukulele", "Bass"];
        this.imgSrc = ["~/assets/instruments/Guitar.png",
            "~/assets/instruments/Ukulele.png", "~/assets/instruments/Bass.png"];
        this.guitarSoundsStandard = {
            "E1": sound.create("~/sounds/guitar_notes/standard/e1.mp3"),
            "A": sound.create("~/sounds/guitar_notes/standard/a.mp3"),
            "D": sound.create("~/sounds/guitar_notes/standard/d.mp3"),
            "G": sound.create("~/sounds/guitar_notes/standard/g.mp3"),
            "B": sound.create("~/sounds/guitar_notes/standard/b.mp3"),
            "E": sound.create("~/sounds/guitar_notes/standard/e2.mp3"),
        };
        this.ukuleleSoundsStandard = {
            "G": sound.create("~/sounds/ukulele_notes/standard/g.mp3"),
            "C": sound.create("~/sounds/ukulele_notes/standard/c.mp3"),
            "E": sound.create("~/sounds/ukulele_notes/standard/e.mp3"),
            "A": sound.create("~/sounds/ukulele_notes/standard/a.mp3"),
        };
        this.bassSoundsStandard = {
            "E": sound.create("~/sounds/bass_notes/standard/e.mp3"),
            "A": sound.create("~/sounds/bass_notes/standard/a.mp3"),
            "D": sound.create("~/sounds/bass_notes/standard/d.mp3"),
            "G": sound.create("~/sounds/bass_notes/standard/g.mp3"),
        };
        this.selectedIndex = 1;
        this.guitarTuning = "Standard";
        this.ukuleleTuning = "Standard";
        this.bassTuning = "Standard";
        this.ukuleleTunings = ["Standard Tuning: G, C, E, A", "Soprano Tuning: A4, D4, F#4, B4",
            "Concert Tuning: A4, D4, F#4, B4", "Tenor Tuning: G4 C4 E4 A4", "Concert Baritone: D3, G3, B3, E4",];
        page.actionBarHidden = true;
        page.statusBarStyle = "light";
        // this.createAdmobBanner();
        // this.createAdmobInsertion();
    }
    HomeComponent.prototype.ngAfterViewChecked = function () {
    };
    HomeComponent.prototype.createAdmobBanner = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createBanner({
                // if this 'view' property is not set, the banner is overlayed on the current top most view
                // view: ..,
                testing: true,
                size: admob.AD_SIZE.SMART_BANNER,
                iosBannerId: "ca-app-pub-3940256099942544/6300978111",
                androidBannerId: "ca-app-pub-3940256099942544/6300978111",
                // androidBannerId: "ca-app-pub-4426146470477887~9760560634", // Real ID
                // Android automatically adds the connected device as test device with testing:true, iOS does not
                iosTestDeviceIds: ["yourTestDeviceUDIDs", "canBeAddedHere"],
                margins: {
                    // if both are set, top wins
                    // top: 10,
                    bottom: 0
                }
            }).then(function () {
                console.log("admob createBanner done");
            }, function (error) {
                console.log("admob createBanner error: " + error);
            });
        }, 2000);
    };
    HomeComponent.prototype.hideAdmobBanner = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            // the .then(.. bit is optional btw
            admob.hideBanner().then(function () {
                console.log("admob hideBanner done");
            }, function (error) {
                console.log("admob hideBanner error: " + error);
            });
        }, 3000);
    };
    HomeComponent.prototype.createAdmobInsertion = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createInterstitial({
                testing: true,
                iosInterstitialId: "ca-app-pub-3940256099942544/6300978111",
                androidInterstitialId: "ca-app-pub-3940256099942544/1033173712",
            }).then(function () {
                console.log("admob createInterstitial done");
            }, function (error) {
                console.log("admob createInterstitial error: " + error);
            });
        }, 10000);
    };
    HomeComponent.prototype.tabChange = function (event) {
        console.log("Tab " + this.tabSelectedIndex + " Selected");
    };
    HomeComponent.prototype.guitarNoteTap = function (note) {
        console.log("Guitar Note Tapped: " + note);
        this.guitarSoundsStandard[note].reset();
        this.guitarSoundsStandard[note].play();
    };
    HomeComponent.prototype.ukuleleNoteTap = function (note) {
        console.log("Guitar Note Tapped: " + note);
        this.ukuleleSoundsStandard[note].reset();
        this.ukuleleSoundsStandard[note].play();
    };
    HomeComponent.prototype.bassNoteTap = function (note) {
        console.log("Bass Note Tapped: " + note);
        this.bassSoundsStandard[note].reset();
        this.bassSoundsStandard[note].play();
    };
    HomeComponent.prototype.settings = function () {
        this.routerExtensions.navigate(['settings']), {
            transition: {
                name: "slideTop",
                duration: 2000,
                curve: "linear"
            }
        };
    };
    HomeComponent.prototype.onchange = function (args) {
        console.log("Drop Down selected index changed from " + args.oldIndex + " to " + args.newIndex);
        switch (args.newIndex) {
            case 0: {
                this.ukuleleTuning = "Standard";
                break;
            }
            case 1: {
                this.ukuleleTuning = "Soprano";
                break;
            }
            case 2: {
                this.ukuleleTuning = "Concert";
                break;
            }
            case 3: {
                this.ukuleleTuning = "Tenor";
                break;
            }
            case 4: {
                this.ukuleleTuning = "Baritone";
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    };
    HomeComponent.prototype.onopen = function () {
        console.log("Drop Down opened.");
    };
    HomeComponent.prototype.onclose = function () {
        console.log("Drop Down closed.");
    };
    HomeComponent.prototype.loopChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            console.log("Checked");
            this.loopSound = true;
        }
        else {
            console.log("Un-Checked");
            this.loopSound = false;
        }
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "ns-home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['./home.css']
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
// ukuleleNoteTap(note: string) {
//     console.log("Ukulele Note Tapped: " + note);
//     this.ukuleleSoundsStandard[note].reset();
//     this.ukuleleSoundsStandard[note].play();
// }
// ukuleleSoundsStandard = {
//     "G": sound.create("~/sounds/ukulele_notes/standard/g.mp3"),
//     "C": sound.create("~/sounds/ukulele_notes/standard/c.mp3"),
//     "E": sound.create("~/sounds/ukulele_notes/standard/e.mp3"),
//     "A": sound.create("~/sounds/ukulele_notes/standard/a.mp3"),
// }; 
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFvRTtBQUNwRSxzREFBK0Q7QUFFL0QsZ0NBQStCO0FBUS9CLCtDQUErQztBQUUvQyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQztBQVExQztJQWdESSx1QkFBb0IsZ0JBQWtDLEVBQVUsSUFBVTtRQUF0RCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBTTtRQTlDMUUscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBQzdCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFFM0Isc0NBQXNDO1FBQ3RDLGdCQUFXLEdBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZELGlCQUFZLEdBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM5QyxjQUFTLEdBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUUzQyxnQkFBVyxHQUFhLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN0RCxXQUFNLEdBQWEsQ0FBQyxpQ0FBaUM7WUFDakQsa0NBQWtDLEVBQUUsK0JBQStCLENBQUMsQ0FBQztRQUd6RSx5QkFBb0IsR0FBRztZQUNuQixJQUFJLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyx1Q0FBdUMsQ0FBQztZQUMzRCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUN6RCxHQUFHLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyx1Q0FBdUMsQ0FBQztTQUM3RCxDQUFDO1FBRUgsMEJBQXFCLEdBQUc7WUFDbkIsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7WUFDMUQsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7WUFDMUQsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7WUFDMUQsR0FBRyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsdUNBQXVDLENBQUM7U0FDN0QsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ2pCLEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLG9DQUFvQyxDQUFDO1NBQzFELENBQUM7UUFFSyxrQkFBYSxHQUFHLENBQUMsQ0FBQztRQUt6QixpQkFBWSxHQUFXLFVBQVUsQ0FBQztRQUNsQyxrQkFBYSxHQUFXLFVBQVUsQ0FBQztRQUNuQyxlQUFVLEdBQVcsVUFBVSxDQUFDO1FBSTVCLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxpQ0FBaUM7WUFDbkYsaUNBQWlDLEVBQUUsMkJBQTJCLEVBQUUsa0NBQWtDLEVBQUUsQ0FBQztRQUN6RyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLE9BQU8sQ0FBQztRQUU5Qiw0QkFBNEI7UUFDNUIsK0JBQStCO0lBQ25DLENBQUM7SUFFRCwwQ0FBa0IsR0FBbEI7SUFDQSxDQUFDO0lBRUQseUNBQWlCLEdBQWpCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDMUMsVUFBVSxDQUFDO1lBQ1AsS0FBSyxDQUFDLFlBQVksQ0FBQztnQkFDZiwyRkFBMkY7Z0JBQzNGLFlBQVk7Z0JBQ1osT0FBTyxFQUFFLElBQUk7Z0JBQ2IsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWTtnQkFDaEMsV0FBVyxFQUFFLHdDQUF3QztnQkFDckQsZUFBZSxFQUFFLHdDQUF3QztnQkFDekQsd0VBQXdFO2dCQUN4RSxpR0FBaUc7Z0JBQ2pHLGdCQUFnQixFQUFFLENBQUMscUJBQXFCLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQzNELE9BQU8sRUFBRTtvQkFDTCw0QkFBNEI7b0JBQzVCLFdBQVc7b0JBQ1gsTUFBTSxFQUFFLENBQUM7aUJBQ1o7YUFDSixDQUFDLENBQUMsSUFBSSxDQUNIO2dCQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQ0QsVUFBVSxLQUFLO2dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDdEQsQ0FBQyxDQUNKLENBQUM7UUFDTixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsdUNBQWUsR0FBZjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLG1DQUFtQztZQUNuQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUNuQjtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUNELFVBQVUsS0FBSztnQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBRWIsQ0FBQztJQUVELDRDQUFvQixHQUFwQjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQztnQkFDckIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsaUJBQWlCLEVBQUUsd0NBQXdDO2dCQUMzRCxxQkFBcUIsRUFBRSx3Q0FBd0M7YUFJbEUsQ0FBQyxDQUFDLElBQUksQ0FDSDtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUM7WUFDakQsQ0FBQyxFQUNELFVBQVUsS0FBSztnQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzVELENBQUMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2QsQ0FBQztJQUVELGlDQUFTLEdBQVQsVUFBVSxLQUFLO1FBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxxQ0FBYSxHQUFiLFVBQWMsSUFBWTtRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVELHNDQUFjLEdBQWQsVUFBZSxJQUFZO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQsbUNBQVcsR0FBWCxVQUFZLElBQVk7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRCxnQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUU7WUFDMUMsVUFBVSxFQUFFO2dCQUNSLElBQUksRUFBRSxVQUFVO2dCQUNoQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxLQUFLLEVBQUUsUUFBUTthQUNsQjtTQUNKLENBQUM7SUFDTixDQUFDO0lBRU0sZ0NBQVEsR0FBZixVQUFnQixJQUFtQztRQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLDJDQUF5QyxJQUFJLENBQUMsUUFBUSxZQUFPLElBQUksQ0FBQyxRQUFVLENBQUMsQ0FBQztRQUMxRixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNwQixLQUFLLENBQUMsRUFBRSxDQUFDO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDO2dCQUNoQyxLQUFLLENBQUM7WUFDVixDQUFDO1lBQ0QsS0FBSyxDQUFDLEVBQUUsQ0FBQztnQkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztnQkFDL0IsS0FBSyxDQUFDO1lBQ1YsQ0FBQztZQUNELEtBQUssQ0FBQyxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7Z0JBQy9CLEtBQUssQ0FBQztZQUNWLENBQUM7WUFDRCxLQUFLLENBQUMsRUFBRSxDQUFDO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDO2dCQUM3QixLQUFLLENBQUM7WUFDVixDQUFDO1lBQ0QsS0FBSyxDQUFDLEVBQUUsQ0FBQztnQkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQztnQkFDaEMsS0FBSyxDQUFDO1lBQ1YsQ0FBQztZQUNELFNBQVMsQ0FBQztnQkFDTixjQUFjO2dCQUNkLEtBQUssQ0FBQztZQUNWLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVNLDhCQUFNLEdBQWI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLCtCQUFPLEdBQWQ7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLG1DQUFXLEdBQWxCLFVBQW1CLElBQUk7UUFDbkIsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQzFCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQztJQUNMLENBQUM7SUE5TVEsYUFBYTtRQU56QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFNBQVM7WUFDbkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFFLENBQUMsWUFBWSxDQUFDO1NBQzVCLENBQUM7eUNBaUR3Qyx5QkFBZ0IsRUFBZ0IsV0FBSTtPQWhEakUsYUFBYSxDQStNekI7SUFBRCxvQkFBQztDQUFBLEFBL01ELElBK01DO0FBL01ZLHNDQUFhO0FBb050QixpQ0FBaUM7QUFDakMsbURBQW1EO0FBQ25ELGdEQUFnRDtBQUNoRCwrQ0FBK0M7QUFDL0MsSUFBSTtBQUdKLDRCQUE0QjtBQUM1QixrRUFBa0U7QUFDbEUsa0VBQWtFO0FBQ2xFLGtFQUFrRTtBQUNsRSxrRUFBa0U7QUFDbEUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBZnRlclZpZXdDaGVja2VkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0ICogYXMgYXBwIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uXCI7XG5pbXBvcnQgKiBhcyBwbGF0Zm9ybSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9wbGF0Zm9ybVwiO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcInVpL3N3aXRjaFwiO1xuXG4vL1BsdWdpbnNcbmltcG9ydCB7IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd25cIjtcbmltcG9ydCB7IE5nU3dpdGNoQ2FzZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcbi8vIGltcG9ydCAqIGFzIEFkbW9iIGZyb20gXCJuYXRpdmVzY3JpcHQtYWRtb2JcIjtcblxudmFyIHNvdW5kID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1zb3VuZFwiKTtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtaG9tZVwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ob21lLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbJy4vaG9tZS5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3Q2hlY2tlZCB7XG5cbiAgICB0YWJTZWxlY3RlZEluZGV4OiBudW1iZXIgPSAwO1xuICAgIGxvb3BTb3VuZDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLy8gZ3VpdGFyTm90ZXM6IHN0cmluZ1tdID0gW1wiRVwiLCBcIkFcIl07XG4gICAgZ3VpdGFyTm90ZXM6IHN0cmluZ1tdID0gWydFJywgJ0EnLCAnRCcsICdHJywgJ0InLCAnRSddO1xuICAgIHVrdWxlbGVOb3Rlczogc3RyaW5nW10gPSBbJ0cnLCAnQycsICdFJywgJ0EnXTtcbiAgICBiYXNzTm90ZXM6IHN0cmluZ1tdID0gWydFJywgJ0EnLCAnRCcsICdHJ107XG5cbiAgICBpbnN0cnVtZW50czogc3RyaW5nW10gPSBbXCJHdWl0YXJcIiwgXCJVa3VsZWxlXCIsIFwiQmFzc1wiXTtcbiAgICBpbWdTcmM6IHN0cmluZ1tdID0gW1wifi9hc3NldHMvaW5zdHJ1bWVudHMvR3VpdGFyLnBuZ1wiLFxuICAgICAgICBcIn4vYXNzZXRzL2luc3RydW1lbnRzL1VrdWxlbGUucG5nXCIsIFwifi9hc3NldHMvaW5zdHJ1bWVudHMvQmFzcy5wbmdcIl07XG5cblxuICAgIGd1aXRhclNvdW5kc1N0YW5kYXJkID0ge1xuICAgICAgICBcIkUxXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL2d1aXRhcl9ub3Rlcy9zdGFuZGFyZC9lMS5tcDNcIiksXG4gICAgICAgIFwiQVwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvYS5tcDNcIiksXG4gICAgICAgIFwiRFwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZC5tcDNcIiksXG4gICAgICAgIFwiR1wiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZy5tcDNcIiksXG4gICAgICAgIFwiQlwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvYi5tcDNcIiksXG4gICAgICAgIFwiRVwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZTIubXAzXCIpLFxuICAgIH07XG5cbiAgIHVrdWxlbGVTb3VuZHNTdGFuZGFyZCA9IHtcbiAgICAgICAgXCJHXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL3VrdWxlbGVfbm90ZXMvc3RhbmRhcmQvZy5tcDNcIiksXG4gICAgICAgIFwiQ1wiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy91a3VsZWxlX25vdGVzL3N0YW5kYXJkL2MubXAzXCIpLFxuICAgICAgICBcIkVcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9lLm1wM1wiKSxcbiAgICAgICAgXCJBXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL3VrdWxlbGVfbm90ZXMvc3RhbmRhcmQvYS5tcDNcIiksXG4gICAgfTtcblxuICAgIGJhc3NTb3VuZHNTdGFuZGFyZCA9IHtcbiAgICAgICAgXCJFXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL2Jhc3Nfbm90ZXMvc3RhbmRhcmQvZS5tcDNcIiksXG4gICAgICAgIFwiQVwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy9iYXNzX25vdGVzL3N0YW5kYXJkL2EubXAzXCIpLFxuICAgICAgICBcIkRcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvYmFzc19ub3Rlcy9zdGFuZGFyZC9kLm1wM1wiKSxcbiAgICAgICAgXCJHXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL2Jhc3Nfbm90ZXMvc3RhbmRhcmQvZy5tcDNcIiksXG4gICAgfTtcblxuICAgIHB1YmxpYyBzZWxlY3RlZEluZGV4ID0gMTtcbiAgICBwdWJsaWMgZ3VpdGFyVHVuaW5nczogQXJyYXk8c3RyaW5nPjtcbiAgICBwdWJsaWMgdWt1bGVsZVR1bmluZ3M6IEFycmF5PHN0cmluZz47XG4gICAgcHVibGljIGJhc3NUdW5pbmdzOiBBcnJheTxzdHJpbmc+O1xuXG4gICAgZ3VpdGFyVHVuaW5nOiBzdHJpbmcgPSBcIlN0YW5kYXJkXCI7XG4gICAgdWt1bGVsZVR1bmluZzogc3RyaW5nID0gXCJTdGFuZGFyZFwiO1xuICAgIGJhc3NUdW5pbmc6IHN0cmluZyA9IFwiU3RhbmRhcmRcIjtcblxuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHtcbiAgICAgICAgdGhpcy51a3VsZWxlVHVuaW5ncyA9IFtcIlN0YW5kYXJkIFR1bmluZzogRywgQywgRSwgQVwiLCBcIlNvcHJhbm8gVHVuaW5nOiBBNCwgRDQsIEYjNCwgQjRcIixcbiAgICAgICAgICAgIFwiQ29uY2VydCBUdW5pbmc6IEE0LCBENCwgRiM0LCBCNFwiLCBcIlRlbm9yIFR1bmluZzogRzQgQzQgRTQgQTRcIiwgXCJDb25jZXJ0IEJhcml0b25lOiBEMywgRzMsIEIzLCBFNFwiLF07XG4gICAgICAgIHBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcbiAgICAgICAgcGFnZS5zdGF0dXNCYXJTdHlsZSA9IFwibGlnaHRcIjtcblxuICAgICAgICAvLyB0aGlzLmNyZWF0ZUFkbW9iQmFubmVyKCk7XG4gICAgICAgIC8vIHRoaXMuY3JlYXRlQWRtb2JJbnNlcnRpb24oKTtcbiAgICB9XG5cbiAgICBuZ0FmdGVyVmlld0NoZWNrZWQoKTogdm9pZCB7XG4gICAgfVxuXG4gICAgY3JlYXRlQWRtb2JCYW5uZXIoKSB7XG4gICAgICAgIHZhciBhZG1vYiA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtYWRtb2JcIik7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgYWRtb2IuY3JlYXRlQmFubmVyKHtcbiAgICAgICAgICAgICAgICAvLyBpZiB0aGlzICd2aWV3JyBwcm9wZXJ0eSBpcyBub3Qgc2V0LCB0aGUgYmFubmVyIGlzIG92ZXJsYXllZCBvbiB0aGUgY3VycmVudCB0b3AgbW9zdCB2aWV3XG4gICAgICAgICAgICAgICAgLy8gdmlldzogLi4sXG4gICAgICAgICAgICAgICAgdGVzdGluZzogdHJ1ZSwgLy8gc2V0IHRvIGZhbHNlIHRvIGdldCByZWFsIGJhbm5lcnNcbiAgICAgICAgICAgICAgICBzaXplOiBhZG1vYi5BRF9TSVpFLlNNQVJUX0JBTk5FUiwgLy8gYW55dGhpbmcgaW4gYWRtb2IuQURfU0laRSwgbGlrZSBhZG1vYi5BRF9TSVpFLlNNQVJUX0JBTk5FUlxuICAgICAgICAgICAgICAgIGlvc0Jhbm5lcklkOiBcImNhLWFwcC1wdWItMzk0MDI1NjA5OTk0MjU0NC82MzAwOTc4MTExXCIsIC8vIGFkZCB5b3VyIG93blxuICAgICAgICAgICAgICAgIGFuZHJvaWRCYW5uZXJJZDogXCJjYS1hcHAtcHViLTM5NDAyNTYwOTk5NDI1NDQvNjMwMDk3ODExMVwiLCAvLyBUZXN0IElEXG4gICAgICAgICAgICAgICAgLy8gYW5kcm9pZEJhbm5lcklkOiBcImNhLWFwcC1wdWItNDQyNjE0NjQ3MDQ3Nzg4N345NzYwNTYwNjM0XCIsIC8vIFJlYWwgSURcbiAgICAgICAgICAgICAgICAvLyBBbmRyb2lkIGF1dG9tYXRpY2FsbHkgYWRkcyB0aGUgY29ubmVjdGVkIGRldmljZSBhcyB0ZXN0IGRldmljZSB3aXRoIHRlc3Rpbmc6dHJ1ZSwgaU9TIGRvZXMgbm90XG4gICAgICAgICAgICAgICAgaW9zVGVzdERldmljZUlkczogW1wieW91clRlc3REZXZpY2VVRElEc1wiLCBcImNhbkJlQWRkZWRIZXJlXCJdLFxuICAgICAgICAgICAgICAgIG1hcmdpbnM6IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgYm90aCBhcmUgc2V0LCB0b3Agd2luc1xuICAgICAgICAgICAgICAgICAgICAvLyB0b3A6IDEwLFxuICAgICAgICAgICAgICAgICAgICBib3R0b206IDBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJhZG1vYiBjcmVhdGVCYW5uZXIgZG9uZVwiKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImFkbW9iIGNyZWF0ZUJhbm5lciBlcnJvcjogXCIgKyBlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSwgMjAwMCk7XG4gICAgfVxuXG4gICAgaGlkZUFkbW9iQmFubmVyKCkge1xuICAgICAgICB2YXIgYWRtb2IgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWFkbW9iXCIpO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vIHRoZSAudGhlbiguLiBiaXQgaXMgb3B0aW9uYWwgYnR3XG4gICAgICAgICAgICBhZG1vYi5oaWRlQmFubmVyKCkudGhlbihcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgaGlkZUJhbm5lciBkb25lXCIpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgaGlkZUJhbm5lciBlcnJvcjogXCIgKyBlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSwgMzAwMCk7XG5cbiAgICB9XG5cbiAgICBjcmVhdGVBZG1vYkluc2VydGlvbigpIHtcbiAgICAgICAgdmFyIGFkbW9iID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hZG1vYlwiKTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBhZG1vYi5jcmVhdGVJbnRlcnN0aXRpYWwoe1xuICAgICAgICAgICAgICAgIHRlc3Rpbmc6IHRydWUsXG4gICAgICAgICAgICAgICAgaW9zSW50ZXJzdGl0aWFsSWQ6IFwiY2EtYXBwLXB1Yi0zOTQwMjU2MDk5OTQyNTQ0LzYzMDA5NzgxMTFcIiwgLy8gYWRkIHlvdXIgb3duXG4gICAgICAgICAgICAgICAgYW5kcm9pZEludGVyc3RpdGlhbElkOiBcImNhLWFwcC1wdWItMzk0MDI1NjA5OTk0MjU0NC8xMDMzMTczNzEyXCIsIC8vIFRlc3QgSURcbiAgICAgICAgICAgICAgICAvLyBhbmRyb2lkSW50ZXJzdGl0aWFsSWQ6IFwiY2EtYXBwLXB1Yi00NDI2MTQ2NDcwNDc3ODg3fjk3NjA1NjA2MzRcIiwgLy8gUmVhbCBJRFxuICAgICAgICAgICAgICAgIC8vIEFuZHJvaWQgYXV0b21hdGljYWxseSBhZGRzIHRoZSBjb25uZWN0ZWQgZGV2aWNlIGFzIHRlc3QgZGV2aWNlIHdpdGggdGVzdGluZzp0cnVlLCBpT1MgZG9lcyBub3RcbiAgICAgICAgICAgICAgICAvLyBpb3NUZXN0RGV2aWNlSWRzOiBbXCJjZTk3MzMwMTMwYzkwNDdjZTBkNDQzMGQzN2Q3MTNiMlwiXVxuICAgICAgICAgICAgfSkudGhlbihcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgY3JlYXRlSW50ZXJzdGl0aWFsIGRvbmVcIik7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJhZG1vYiBjcmVhdGVJbnRlcnN0aXRpYWwgZXJyb3I6IFwiICsgZXJyb3IpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgICAgIH0sIDEwMDAwKTtcbiAgICB9XG5cbiAgICB0YWJDaGFuZ2UoZXZlbnQpOiB2b2lkIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJUYWIgXCIgKyB0aGlzLnRhYlNlbGVjdGVkSW5kZXggKyBcIiBTZWxlY3RlZFwiKTtcbiAgICB9XG5cbiAgICBndWl0YXJOb3RlVGFwKG5vdGU6IHN0cmluZykge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkd1aXRhciBOb3RlIFRhcHBlZDogXCIgKyBub3RlKTtcbiAgICAgICAgdGhpcy5ndWl0YXJTb3VuZHNTdGFuZGFyZFtub3RlXS5yZXNldCgpO1xuICAgICAgICB0aGlzLmd1aXRhclNvdW5kc1N0YW5kYXJkW25vdGVdLnBsYXkoKTtcbiAgICB9XG5cbiAgICB1a3VsZWxlTm90ZVRhcChub3RlOiBzdHJpbmcpe1xuICAgICAgICBjb25zb2xlLmxvZyhcIkd1aXRhciBOb3RlIFRhcHBlZDogXCIgKyBub3RlKTtcbiAgICAgICAgdGhpcy51a3VsZWxlU291bmRzU3RhbmRhcmRbbm90ZV0ucmVzZXQoKTtcbiAgICAgICAgdGhpcy51a3VsZWxlU291bmRzU3RhbmRhcmRbbm90ZV0ucGxheSgpOyBcbiAgICB9XG5cbiAgICBiYXNzTm90ZVRhcChub3RlOiBzdHJpbmcpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJCYXNzIE5vdGUgVGFwcGVkOiBcIiArIG5vdGUpO1xuICAgICAgICB0aGlzLmJhc3NTb3VuZHNTdGFuZGFyZFtub3RlXS5yZXNldCgpO1xuICAgICAgICB0aGlzLmJhc3NTb3VuZHNTdGFuZGFyZFtub3RlXS5wbGF5KCk7XG4gICAgfVxuXG4gICAgc2V0dGluZ3MoKSB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJ3NldHRpbmdzJ10pLCB7XG4gICAgICAgICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJzbGlkZVRvcFwiLFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAyMDAwLFxuICAgICAgICAgICAgICAgIGN1cnZlOiBcImxpbmVhclwiXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcHVibGljIG9uY2hhbmdlKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGBEcm9wIERvd24gc2VsZWN0ZWQgaW5kZXggY2hhbmdlZCBmcm9tICR7YXJncy5vbGRJbmRleH0gdG8gJHthcmdzLm5ld0luZGV4fWApO1xuICAgICAgICBzd2l0Y2ggKGFyZ3MubmV3SW5kZXgpIHtcbiAgICAgICAgICAgIGNhc2UgMDoge1xuICAgICAgICAgICAgICAgIHRoaXMudWt1bGVsZVR1bmluZyA9IFwiU3RhbmRhcmRcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhc2UgMToge1xuICAgICAgICAgICAgICAgIHRoaXMudWt1bGVsZVR1bmluZyA9IFwiU29wcmFub1wiO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2FzZSAyOiB7XG4gICAgICAgICAgICAgICAgdGhpcy51a3VsZWxlVHVuaW5nID0gXCJDb25jZXJ0XCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXNlIDM6IHtcbiAgICAgICAgICAgICAgICB0aGlzLnVrdWxlbGVUdW5pbmcgPSBcIlRlbm9yXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXNlIDQ6IHtcbiAgICAgICAgICAgICAgICB0aGlzLnVrdWxlbGVUdW5pbmcgPSBcIkJhcml0b25lXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkZWZhdWx0OiB7XG4gICAgICAgICAgICAgICAgLy9zdGF0ZW1lbnRzOyBcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBvbm9wZW4oKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRHJvcCBEb3duIG9wZW5lZC5cIik7XG4gICAgfVxuXG4gICAgcHVibGljIG9uY2xvc2UoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRHJvcCBEb3duIGNsb3NlZC5cIik7XG4gICAgfVxuXG4gICAgcHVibGljIGxvb3BDaGVja2VkKGFyZ3MpIHtcbiAgICAgICAgbGV0IGZpcnN0U3dpdGNoID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcbiAgICAgICAgaWYgKGZpcnN0U3dpdGNoLmNoZWNrZWQpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ2hlY2tlZFwiKVxuICAgICAgICAgICAgdGhpcy5sb29wU291bmQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbi1DaGVja2VkXCIpXG4gICAgICAgICAgICB0aGlzLmxvb3BTb3VuZCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5cblxuXG4gICAgLy8gdWt1bGVsZU5vdGVUYXAobm90ZTogc3RyaW5nKSB7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKFwiVWt1bGVsZSBOb3RlIFRhcHBlZDogXCIgKyBub3RlKTtcbiAgICAvLyAgICAgdGhpcy51a3VsZWxlU291bmRzU3RhbmRhcmRbbm90ZV0ucmVzZXQoKTtcbiAgICAvLyAgICAgdGhpcy51a3VsZWxlU291bmRzU3RhbmRhcmRbbm90ZV0ucGxheSgpO1xuICAgIC8vIH1cblxuXG4gICAgLy8gdWt1bGVsZVNvdW5kc1N0YW5kYXJkID0ge1xuICAgIC8vICAgICBcIkdcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9nLm1wM1wiKSxcbiAgICAvLyAgICAgXCJDXCI6IHNvdW5kLmNyZWF0ZShcIn4vc291bmRzL3VrdWxlbGVfbm90ZXMvc3RhbmRhcmQvYy5tcDNcIiksXG4gICAgLy8gICAgIFwiRVwiOiBzb3VuZC5jcmVhdGUoXCJ+L3NvdW5kcy91a3VsZWxlX25vdGVzL3N0YW5kYXJkL2UubXAzXCIpLFxuICAgIC8vICAgICBcIkFcIjogc291bmQuY3JlYXRlKFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9hLm1wM1wiKSxcbiAgICAvLyB9OyJdfQ==