import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { Page } from "ui/page";
import * as app from "tns-core-modules/application";
import * as platform from "tns-core-modules/platform";

var sound = require("nativescript-sound");

@Component({
    selector: "ns-settings",
    moduleId: module.id,
    templateUrl: "./settings.component.html",
    styleUrls: ['./settings.css']
})
export class SettingsComponent implements OnInit {

    selected: number = 0;

    constructor(private routerExtensions: RouterExtensions, private page: Page) {
        page.actionBarHidden = true;
    }

    ngOnInit(): void {

    }

    instrumentTap(itemTapped: number) {
        this.selected = itemTapped;
    }

    done() {
        this.routerExtensions.navigate(['/home']);
    }

    logout(){
        this.routerExtensions.navigate(['/login']);
 
    }
}
