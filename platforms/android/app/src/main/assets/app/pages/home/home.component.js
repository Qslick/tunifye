"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var nativescript_audio_1 = require("nativescript-audio");
var application_1 = require("application");
// var sound = require("nativescript-sound");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(routerExtensions, page) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.tabSelectedIndex = 0;
        this.loopSound = false;
        // guitarNotes: string[] = ["E", "A"];
        this.guitarNotes = ['E', 'A', 'D', 'G', 'B', 'E2'];
        this.ukuleleNotes = ['G', 'C', 'E', 'A'];
        this.bassNotes = ['E', 'A', 'D', 'G'];
        this.instruments = ["Guitar", "Ukulele", "Bass"];
        this.imgSrc = ["~/assets/instruments/Guitar.png",
            "~/assets/instruments/Ukulele.png", "~/assets/instruments/Bass.png"];
        this.guitarSoundsStandard = {
            "E2": "~/sounds/guitar_notes/standard/e1.mp3",
            "A": "~/sounds/guitar_notes/standard/a.mp3",
            "D": "~/sounds/guitar_notes/standard/d.mp3",
            "G": "~/sounds/guitar_notes/standard/g.mp3",
            "B": "~/sounds/guitar_notes/standard/b.mp3",
            "E": "~/sounds/guitar_notes/standard/e2.mp3",
        };
        this.ukuleleSoundsStandard = {
            "G": "~/sounds/ukulele_notes/standard/g.mp3",
            "C": "~/sounds/ukulele_notes/standard/c.mp3",
            "E": "~/sounds/ukulele_notes/standard/e.mp3",
            "A": "~/sounds/ukulele_notes/standard/a.mp3",
        };
        this.bassSoundsStandard = {
            "E": "~/sounds/bass_notes/standard/e.mp3",
            "A": "~/sounds/bass_notes/standard/a.mp3",
            "D": "~/sounds/bass_notes/standard/d.mp3",
            "G": "~/sounds/bass_notes/standard/g.mp3",
        };
        this.selectedIndex = 1;
        this.guitarTuning = "Standard";
        this.ukuleleTuning = "Standard";
        this.bassTuning = "Standard";
        this.ukuleleTunings = ["Standard Tuning: G, C, E, A", "Soprano Tuning: A4, D4, F#4, B4",
            "Concert Tuning: A4, D4, F#4, B4", "Tenor Tuning: G4 C4 E4 A4", "Concert Baritone: D3, G3, B3, E4",];
        page.actionBarHidden = true;
        page.statusBarStyle = "light";
        this._player = new nativescript_audio_1.TNSPlayer();
        this._player.debug = true; // set true to enable TNSPlayer console logs for debugging.
        this._player
            .initFromFile({
            audioFile: "~/sounds/guitar_notes/standard/a.mp3",
            loop: false,
            completeCallback: this._trackComplete.bind(this),
            errorCallback: this._trackError.bind(this)
        })
            .then(function () {
            _this._player.getAudioTrackDuration().then(function (duration) {
                // iOS: duration is in seconds
                // Android: duration is in milliseconds
                console.log("song duration:", duration);
            });
        });
        application_1.on(application_1.suspendEvent, function (args) {
            if (args.android) {
                _this._player.dispose();
                console.log("Activity: " + args.android);
            }
            else if (args.ios) {
                _this._player.dispose();
                // For iOS applications, args.ios is UIApplication.
                console.log("UIApplication: " + args.ios);
            }
        });
        this.createAdmobBanner();
        this.createAdmobInsertion();
    }
    HomeComponent.prototype.ngAfterViewChecked = function () {
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        console.log("DESTROYED");
    };
    HomeComponent.prototype._trackComplete = function (args) {
        console.log("reference back to player:", args.player);
        // iOS only: flag indicating if completed succesfully
        console.log("whether song play completed successfully:", args.flag);
    };
    HomeComponent.prototype._trackError = function (args) {
        console.log("reference back to player:", args.player);
        console.log("the error:", args.error);
        // Android only: extra detail on error
        console.log("extra info on the error:", args.extra);
    };
    HomeComponent.prototype.createAdmobBanner = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createBanner({
                // if this 'view' property is not set, the banner is overlayed on the current top most view
                // view: ..,
                testing: false,
                size: admob.AD_SIZE.SMART_BANNER,
                iosBannerId: "ca-app-pub-3940256099942544/6300978111",
                androidBannerId: "ca-app-pub-4426146470477887/7681192204",
                // androidBannerId: "ca-app-pub-4426146470477887~9760560634", // Real ID
                // Android automatically adds the connected device as test device with testing:true, iOS does not
                // iosTestDeviceIds: ["yourTestDeviceUDIDs", "canBeAddedHere"],
                margins: {
                    // if both are set, top wins
                    // top: 10,
                    bottom: 0
                }
            }).then(function () {
                console.log("admob createBanner done");
            }, function (error) {
                console.log("admob createBanner error: " + error);
            });
        }, 2000);
    };
    HomeComponent.prototype.hideAdmobBanner = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            // the .then(.. bit is optional btw
            admob.hideBanner().then(function () {
                console.log("admob hideBanner done");
            }, function (error) {
                console.log("admob hideBanner error: " + error);
            });
        }, 3000);
    };
    HomeComponent.prototype.createAdmobInsertion = function () {
        var admob = require("nativescript-admob");
        setTimeout(function () {
            admob.createInterstitial({
                testing: false,
                iosInterstitialId: "ca-app-pub-3940256099942544/6300978111",
                androidInterstitialId: "ca-app-pub-4426146470477887/2875080900",
            }).then(function () {
                console.log("admob createInterstitial done");
            }, function (error) {
                console.log("admob createInterstitial error: " + error);
            });
        }, 180000);
    };
    HomeComponent.prototype.finishedPlaying = function () {
    };
    HomeComponent.prototype.tabChange = function (event) {
        console.log("Tab " + this.tabSelectedIndex + " Selected");
    };
    HomeComponent.prototype.guitarNoteTap = function (note) {
        console.log("Guitar Note Tapped: " + note);
        this.activeNote = note;
        this._player.playFromFile({
            audioFile: this.guitarSoundsStandard[note],
            loop: this.loopSound,
            completeCallback: this._trackComplete.bind(this),
            errorCallback: this._trackError.bind(this)
        });
    };
    HomeComponent.prototype.ukuleleNoteTap = function (note) {
        console.log("Guitar Note Tapped: " + note);
        this.activeNote = note;
        this._player.playFromFile({
            audioFile: this.ukuleleSoundsStandard[note],
            loop: this.loopSound,
            completeCallback: this._trackComplete.bind(this),
            errorCallback: this._trackError.bind(this)
        });
    };
    HomeComponent.prototype.bassNoteTap = function (note) {
        console.log("Bass Note Tapped: " + note);
        this.activeNote = note;
        this._player.playFromFile({
            audioFile: this.bassSoundsStandard[note],
            loop: this.loopSound,
            completeCallback: this._trackComplete.bind(this),
            errorCallback: this._trackError.bind(this)
        });
    };
    HomeComponent.prototype.settings = function () {
        this.routerExtensions.navigate(['settings']), {
            transition: {
                name: "slideTop",
                duration: 2000,
                curve: "linear"
            }
        };
    };
    HomeComponent.prototype.onchange = function (args) {
        console.log("Drop Down selected index changed from " + args.oldIndex + " to " + args.newIndex);
        switch (args.newIndex) {
            case 0: {
                this.ukuleleTuning = "Standard";
                break;
            }
            case 1: {
                this.ukuleleTuning = "Soprano";
                break;
            }
            case 2: {
                this.ukuleleTuning = "Concert";
                break;
            }
            case 3: {
                this.ukuleleTuning = "Tenor";
                break;
            }
            case 4: {
                this.ukuleleTuning = "Baritone";
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    };
    HomeComponent.prototype.onopen = function () {
        console.log("Drop Down opened.");
    };
    HomeComponent.prototype.onclose = function () {
        console.log("Drop Down closed.");
    };
    HomeComponent.prototype.loopChecked = function (args) {
        var firstSwitch = args.object;
        if (firstSwitch.checked) {
            console.log("Checked");
            this.loopSound = true;
        }
        else {
            console.log("Un-Checked");
            this.loopSound = false;
            this.activeNote = '';
            this._player.pause();
        }
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "ns-home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['./home.css']
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions, page_1.Page])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUErRTtBQUMvRSxzREFBK0Q7QUFFL0QsZ0NBQStCO0FBUS9CLHlEQUErQztBQUUvQywyQ0FBMEw7QUFFMUwsNkNBQTZDO0FBUTdDO0lBb0RJLHVCQUFvQixnQkFBa0MsRUFBVSxJQUFVO1FBQTFFLGlCQXNDQztRQXRDbUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFsRDFFLHFCQUFnQixHQUFXLENBQUMsQ0FBQztRQUM3QixjQUFTLEdBQVksS0FBSyxDQUFDO1FBRTNCLHNDQUFzQztRQUN0QyxnQkFBVyxHQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4RCxpQkFBWSxHQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUMsY0FBUyxHQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFM0MsZ0JBQVcsR0FBYSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDdEQsV0FBTSxHQUFhLENBQUMsaUNBQWlDO1lBQ2pELGtDQUFrQyxFQUFFLCtCQUErQixDQUFDLENBQUM7UUFHekUseUJBQW9CLEdBQUc7WUFDbkIsSUFBSSxFQUFFLHVDQUF1QztZQUM3QyxHQUFHLEVBQUUsc0NBQXNDO1lBQzNDLEdBQUcsRUFBRSxzQ0FBc0M7WUFDM0MsR0FBRyxFQUFFLHNDQUFzQztZQUMzQyxHQUFHLEVBQUUsc0NBQXNDO1lBQzNDLEdBQUcsRUFBRSx1Q0FBdUM7U0FDL0MsQ0FBQztRQUVGLDBCQUFxQixHQUFHO1lBQ3BCLEdBQUcsRUFBRSx1Q0FBdUM7WUFDNUMsR0FBRyxFQUFFLHVDQUF1QztZQUM1QyxHQUFHLEVBQUUsdUNBQXVDO1lBQzVDLEdBQUcsRUFBRSx1Q0FBdUM7U0FDL0MsQ0FBQztRQUVGLHVCQUFrQixHQUFHO1lBQ2pCLEdBQUcsRUFBRSxvQ0FBb0M7WUFDekMsR0FBRyxFQUFFLG9DQUFvQztZQUN6QyxHQUFHLEVBQUUsb0NBQW9DO1lBQ3pDLEdBQUcsRUFBRSxvQ0FBb0M7U0FDNUMsQ0FBQztRQUVLLGtCQUFhLEdBQUcsQ0FBQyxDQUFDO1FBS3pCLGlCQUFZLEdBQVcsVUFBVSxDQUFDO1FBQ2xDLGtCQUFhLEdBQVcsVUFBVSxDQUFDO1FBQ25DLGVBQVUsR0FBVyxVQUFVLENBQUM7UUFRNUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLDZCQUE2QixFQUFFLGlDQUFpQztZQUNuRixpQ0FBaUMsRUFBRSwyQkFBMkIsRUFBRSxrQ0FBa0MsRUFBRSxDQUFDO1FBQ3pHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDO1FBRTlCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSw4QkFBUyxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsMkRBQTJEO1FBQ3RGLElBQUksQ0FBQyxPQUFPO2FBQ1AsWUFBWSxDQUFDO1lBQ1YsU0FBUyxFQUFFLHNDQUFzQztZQUNqRCxJQUFJLEVBQUUsS0FBSztZQUNYLGdCQUFnQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNoRCxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQzdDLENBQUM7YUFDRCxJQUFJLENBQUM7WUFDRixLQUFJLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtnQkFDOUMsOEJBQThCO2dCQUM5Qix1Q0FBdUM7Z0JBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUdQLGdCQUFhLENBQUMsMEJBQVksRUFBRSxVQUFDLElBQTBCO1lBQ25ELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNmLEtBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM3QyxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixLQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN2QixtREFBbUQ7Z0JBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzlDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUdILElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCwwQ0FBa0IsR0FBbEI7SUFDQSxDQUFDO0lBRUQsbUNBQVcsR0FBWDtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVPLHNDQUFjLEdBQXRCLFVBQXVCLElBQVM7UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQscURBQXFEO1FBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQTJDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFTyxtQ0FBVyxHQUFuQixVQUFvQixJQUFTO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0QyxzQ0FBc0M7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELHlDQUFpQixHQUFqQjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLEtBQUssQ0FBQyxZQUFZLENBQUM7Z0JBQ2YsMkZBQTJGO2dCQUMzRixZQUFZO2dCQUNaLE9BQU8sRUFBRSxLQUFLO2dCQUNkLElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVk7Z0JBQ2hDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELGVBQWUsRUFBRSx3Q0FBd0M7Z0JBQ3pELHdFQUF3RTtnQkFDeEUsaUdBQWlHO2dCQUNqRywrREFBK0Q7Z0JBQy9ELE9BQU8sRUFBRTtvQkFDTCw0QkFBNEI7b0JBQzVCLFdBQVc7b0JBQ1gsTUFBTSxFQUFFLENBQUM7aUJBQ1o7YUFDSixDQUFDLENBQUMsSUFBSSxDQUNIO2dCQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQ0QsVUFBVSxLQUFLO2dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDdEQsQ0FBQyxDQUNKLENBQUM7UUFDTixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsdUNBQWUsR0FBZjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLG1DQUFtQztZQUNuQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUNuQjtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUNELFVBQVUsS0FBSztnQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBRWIsQ0FBQztJQUVELDRDQUFvQixHQUFwQjtRQUNJLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQztZQUNQLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQztnQkFDckIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsaUJBQWlCLEVBQUUsd0NBQXdDO2dCQUMzRCxxQkFBcUIsRUFBRSx3Q0FBd0M7YUFJbEUsQ0FBQyxDQUFDLElBQUksQ0FDSDtnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUM7WUFDakQsQ0FBQyxFQUNELFVBQVUsS0FBSztnQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzVELENBQUMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2YsQ0FBQztJQUVELHVDQUFlLEdBQWY7SUFFQSxDQUFDO0lBRUQsaUNBQVMsR0FBVCxVQUFVLEtBQUs7UUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsV0FBVyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHFDQUFhLEdBQWIsVUFBYyxJQUFZO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7WUFDMUMsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3BCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNoRCxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQzdDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQ0FBYyxHQUFkLFVBQWUsSUFBWTtRQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO1lBQ3RCLFNBQVMsRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO1lBQzNDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUztZQUNwQixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDaEQsYUFBYSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUM3QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsbUNBQVcsR0FBWCxVQUFZLElBQVk7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUN0QixTQUFTLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztZQUN4QyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7WUFDcEIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hELGFBQWEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDN0MsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRTtZQUMxQyxVQUFVLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLEtBQUssRUFBRSxRQUFRO2FBQ2xCO1NBQ0osQ0FBQztJQUNOLENBQUM7SUFFTSxnQ0FBUSxHQUFmLFVBQWdCLElBQW1DO1FBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQXlDLElBQUksQ0FBQyxRQUFRLFlBQU8sSUFBSSxDQUFDLFFBQVUsQ0FBQyxDQUFDO1FBQzFGLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssQ0FBQyxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUM7Z0JBQ2hDLEtBQUssQ0FBQztZQUNWLENBQUM7WUFDRCxLQUFLLENBQUMsRUFBRSxDQUFDO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO2dCQUMvQixLQUFLLENBQUM7WUFDVixDQUFDO1lBQ0QsS0FBSyxDQUFDLEVBQUUsQ0FBQztnQkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztnQkFDL0IsS0FBSyxDQUFDO1lBQ1YsQ0FBQztZQUNELEtBQUssQ0FBQyxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7Z0JBQzdCLEtBQUssQ0FBQztZQUNWLENBQUM7WUFDRCxLQUFLLENBQUMsRUFBRSxDQUFDO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDO2dCQUNoQyxLQUFLLENBQUM7WUFDVixDQUFDO1lBQ0QsU0FBUyxDQUFDO2dCQUNOLGNBQWM7Z0JBQ2QsS0FBSyxDQUFDO1lBQ1YsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sOEJBQU0sR0FBYjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRU0sK0JBQU8sR0FBZDtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRU0sbUNBQVcsR0FBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDMUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pCLENBQUM7SUFDTCxDQUFDO0lBdFJRLGFBQWE7UUFOekIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxTQUFTO1lBQ25CLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsdUJBQXVCO1lBQ3BDLFNBQVMsRUFBRSxDQUFDLFlBQVksQ0FBQztTQUM1QixDQUFDO3lDQXFEd0MseUJBQWdCLEVBQWdCLFdBQUk7T0FwRGpFLGFBQWEsQ0F1UnpCO0lBQUQsb0JBQUM7Q0FBQSxBQXZSRCxJQXVSQztBQXZSWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBZnRlclZpZXdDaGVja2VkLCBPbkRlc3Ryb3kgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XG5pbXBvcnQgKiBhcyBhcHAgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIjtcbmltcG9ydCAqIGFzIHBsYXRmb3JtIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCI7XG5pbXBvcnQgeyBTd2l0Y2ggfSBmcm9tIFwidWkvc3dpdGNoXCI7XG5cbi8vUGx1Z2luc1xuaW1wb3J0IHsgU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWRyb3AtZG93blwiO1xuaW1wb3J0IHsgTmdTd2l0Y2hDYXNlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xuaW1wb3J0IHsgVE5TUGxheWVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hdWRpb1wiO1xuXG5pbXBvcnQgeyBvbiBhcyBhcHBsaWNhdGlvbk9uLCBsYXVuY2hFdmVudCwgc3VzcGVuZEV2ZW50LCByZXN1bWVFdmVudCwgZXhpdEV2ZW50LCBsb3dNZW1vcnlFdmVudCwgdW5jYXVnaHRFcnJvckV2ZW50LCBBcHBsaWNhdGlvbkV2ZW50RGF0YSwgc3RhcnQgYXMgYXBwbGljYXRpb25TdGFydCB9IGZyb20gXCJhcHBsaWNhdGlvblwiO1xuXG4vLyB2YXIgc291bmQgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXNvdW5kXCIpO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJucy1ob21lXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2hvbWUuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFsnLi9ob21lLmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdDaGVja2VkLCBPbkRlc3Ryb3kge1xuXG4gICAgdGFiU2VsZWN0ZWRJbmRleDogbnVtYmVyID0gMDtcbiAgICBsb29wU291bmQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8vIGd1aXRhck5vdGVzOiBzdHJpbmdbXSA9IFtcIkVcIiwgXCJBXCJdO1xuICAgIGd1aXRhck5vdGVzOiBzdHJpbmdbXSA9IFsnRScsICdBJywgJ0QnLCAnRycsICdCJywgJ0UyJ107XG4gICAgdWt1bGVsZU5vdGVzOiBzdHJpbmdbXSA9IFsnRycsICdDJywgJ0UnLCAnQSddO1xuICAgIGJhc3NOb3Rlczogc3RyaW5nW10gPSBbJ0UnLCAnQScsICdEJywgJ0cnXTtcblxuICAgIGluc3RydW1lbnRzOiBzdHJpbmdbXSA9IFtcIkd1aXRhclwiLCBcIlVrdWxlbGVcIiwgXCJCYXNzXCJdO1xuICAgIGltZ1NyYzogc3RyaW5nW10gPSBbXCJ+L2Fzc2V0cy9pbnN0cnVtZW50cy9HdWl0YXIucG5nXCIsXG4gICAgICAgIFwifi9hc3NldHMvaW5zdHJ1bWVudHMvVWt1bGVsZS5wbmdcIiwgXCJ+L2Fzc2V0cy9pbnN0cnVtZW50cy9CYXNzLnBuZ1wiXTtcblxuXG4gICAgZ3VpdGFyU291bmRzU3RhbmRhcmQgPSB7XG4gICAgICAgIFwiRTJcIjogXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZTEubXAzXCIsXG4gICAgICAgIFwiQVwiOiBcIn4vc291bmRzL2d1aXRhcl9ub3Rlcy9zdGFuZGFyZC9hLm1wM1wiLFxuICAgICAgICBcIkRcIjogXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZC5tcDNcIixcbiAgICAgICAgXCJHXCI6IFwifi9zb3VuZHMvZ3VpdGFyX25vdGVzL3N0YW5kYXJkL2cubXAzXCIsXG4gICAgICAgIFwiQlwiOiBcIn4vc291bmRzL2d1aXRhcl9ub3Rlcy9zdGFuZGFyZC9iLm1wM1wiLFxuICAgICAgICBcIkVcIjogXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvZTIubXAzXCIsXG4gICAgfTtcblxuICAgIHVrdWxlbGVTb3VuZHNTdGFuZGFyZCA9IHtcbiAgICAgICAgXCJHXCI6IFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9nLm1wM1wiLFxuICAgICAgICBcIkNcIjogXCJ+L3NvdW5kcy91a3VsZWxlX25vdGVzL3N0YW5kYXJkL2MubXAzXCIsXG4gICAgICAgIFwiRVwiOiBcIn4vc291bmRzL3VrdWxlbGVfbm90ZXMvc3RhbmRhcmQvZS5tcDNcIixcbiAgICAgICAgXCJBXCI6IFwifi9zb3VuZHMvdWt1bGVsZV9ub3Rlcy9zdGFuZGFyZC9hLm1wM1wiLFxuICAgIH07XG5cbiAgICBiYXNzU291bmRzU3RhbmRhcmQgPSB7XG4gICAgICAgIFwiRVwiOiBcIn4vc291bmRzL2Jhc3Nfbm90ZXMvc3RhbmRhcmQvZS5tcDNcIixcbiAgICAgICAgXCJBXCI6IFwifi9zb3VuZHMvYmFzc19ub3Rlcy9zdGFuZGFyZC9hLm1wM1wiLFxuICAgICAgICBcIkRcIjogXCJ+L3NvdW5kcy9iYXNzX25vdGVzL3N0YW5kYXJkL2QubXAzXCIsXG4gICAgICAgIFwiR1wiOiBcIn4vc291bmRzL2Jhc3Nfbm90ZXMvc3RhbmRhcmQvZy5tcDNcIixcbiAgICB9O1xuXG4gICAgcHVibGljIHNlbGVjdGVkSW5kZXggPSAxO1xuICAgIHB1YmxpYyBndWl0YXJUdW5pbmdzOiBBcnJheTxzdHJpbmc+O1xuICAgIHB1YmxpYyB1a3VsZWxlVHVuaW5nczogQXJyYXk8c3RyaW5nPjtcbiAgICBwdWJsaWMgYmFzc1R1bmluZ3M6IEFycmF5PHN0cmluZz47XG5cbiAgICBndWl0YXJUdW5pbmc6IHN0cmluZyA9IFwiU3RhbmRhcmRcIjtcbiAgICB1a3VsZWxlVHVuaW5nOiBzdHJpbmcgPSBcIlN0YW5kYXJkXCI7XG4gICAgYmFzc1R1bmluZzogc3RyaW5nID0gXCJTdGFuZGFyZFwiO1xuXG4gICAgYWN0aXZlTm90ZTogc3RyaW5nO1xuXG4gICAgcHJpdmF0ZSBfcGxheWVyOiBUTlNQbGF5ZXI7XG5cblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucywgcHJpdmF0ZSBwYWdlOiBQYWdlKSB7XG4gICAgICAgIHRoaXMudWt1bGVsZVR1bmluZ3MgPSBbXCJTdGFuZGFyZCBUdW5pbmc6IEcsIEMsIEUsIEFcIiwgXCJTb3ByYW5vIFR1bmluZzogQTQsIEQ0LCBGIzQsIEI0XCIsXG4gICAgICAgICAgICBcIkNvbmNlcnQgVHVuaW5nOiBBNCwgRDQsIEYjNCwgQjRcIiwgXCJUZW5vciBUdW5pbmc6IEc0IEM0IEU0IEE0XCIsIFwiQ29uY2VydCBCYXJpdG9uZTogRDMsIEczLCBCMywgRTRcIixdO1xuICAgICAgICBwYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gICAgICAgIHBhZ2Uuc3RhdHVzQmFyU3R5bGUgPSBcImxpZ2h0XCI7XG5cbiAgICAgICAgdGhpcy5fcGxheWVyID0gbmV3IFROU1BsYXllcigpO1xuICAgICAgICB0aGlzLl9wbGF5ZXIuZGVidWcgPSB0cnVlOyAvLyBzZXQgdHJ1ZSB0byBlbmFibGUgVE5TUGxheWVyIGNvbnNvbGUgbG9ncyBmb3IgZGVidWdnaW5nLlxuICAgICAgICB0aGlzLl9wbGF5ZXJcbiAgICAgICAgICAgIC5pbml0RnJvbUZpbGUoe1xuICAgICAgICAgICAgICAgIGF1ZGlvRmlsZTogXCJ+L3NvdW5kcy9ndWl0YXJfbm90ZXMvc3RhbmRhcmQvYS5tcDNcIiwgLy8gfiA9IGFwcCBkaXJlY3RvcnlcbiAgICAgICAgICAgICAgICBsb29wOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBjb21wbGV0ZUNhbGxiYWNrOiB0aGlzLl90cmFja0NvbXBsZXRlLmJpbmQodGhpcyksXG4gICAgICAgICAgICAgICAgZXJyb3JDYWxsYmFjazogdGhpcy5fdHJhY2tFcnJvci5iaW5kKHRoaXMpXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuX3BsYXllci5nZXRBdWRpb1RyYWNrRHVyYXRpb24oKS50aGVuKGR1cmF0aW9uID0+IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gaU9TOiBkdXJhdGlvbiBpcyBpbiBzZWNvbmRzXG4gICAgICAgICAgICAgICAgICAgIC8vIEFuZHJvaWQ6IGR1cmF0aW9uIGlzIGluIG1pbGxpc2Vjb25kc1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgc29uZyBkdXJhdGlvbjpgLCBkdXJhdGlvbik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcblxuXG4gICAgICAgIGFwcGxpY2F0aW9uT24oc3VzcGVuZEV2ZW50LCAoYXJnczogQXBwbGljYXRpb25FdmVudERhdGEpID0+IHtcbiAgICAgICAgICAgIGlmIChhcmdzLmFuZHJvaWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9wbGF5ZXIuZGlzcG9zZSgpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQWN0aXZpdHk6IFwiICsgYXJncy5hbmRyb2lkKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYXJncy5pb3MpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9wbGF5ZXIuZGlzcG9zZSgpO1xuICAgICAgICAgICAgICAgIC8vIEZvciBpT1MgYXBwbGljYXRpb25zLCBhcmdzLmlvcyBpcyBVSUFwcGxpY2F0aW9uLlxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiVUlBcHBsaWNhdGlvbjogXCIgKyBhcmdzLmlvcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgdGhpcy5jcmVhdGVBZG1vYkJhbm5lcigpO1xuICAgICAgICB0aGlzLmNyZWF0ZUFkbW9iSW5zZXJ0aW9uKCk7XG4gICAgfVxuXG4gICAgbmdBZnRlclZpZXdDaGVja2VkKCk6IHZvaWQge1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkRFU1RST1lFRFwiKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF90cmFja0NvbXBsZXRlKGFyZ3M6IGFueSkge1xuICAgICAgICBjb25zb2xlLmxvZyhcInJlZmVyZW5jZSBiYWNrIHRvIHBsYXllcjpcIiwgYXJncy5wbGF5ZXIpO1xuICAgICAgICAvLyBpT1Mgb25seTogZmxhZyBpbmRpY2F0aW5nIGlmIGNvbXBsZXRlZCBzdWNjZXNmdWxseVxuICAgICAgICBjb25zb2xlLmxvZyhcIndoZXRoZXIgc29uZyBwbGF5IGNvbXBsZXRlZCBzdWNjZXNzZnVsbHk6XCIsIGFyZ3MuZmxhZyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfdHJhY2tFcnJvcihhcmdzOiBhbnkpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJyZWZlcmVuY2UgYmFjayB0byBwbGF5ZXI6XCIsIGFyZ3MucGxheWVyKTtcbiAgICAgICAgY29uc29sZS5sb2coXCJ0aGUgZXJyb3I6XCIsIGFyZ3MuZXJyb3IpO1xuICAgICAgICAvLyBBbmRyb2lkIG9ubHk6IGV4dHJhIGRldGFpbCBvbiBlcnJvclxuICAgICAgICBjb25zb2xlLmxvZyhcImV4dHJhIGluZm8gb24gdGhlIGVycm9yOlwiLCBhcmdzLmV4dHJhKTtcbiAgICB9XG5cbiAgICBjcmVhdGVBZG1vYkJhbm5lcigpIHtcbiAgICAgICAgdmFyIGFkbW9iID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hZG1vYlwiKTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBhZG1vYi5jcmVhdGVCYW5uZXIoe1xuICAgICAgICAgICAgICAgIC8vIGlmIHRoaXMgJ3ZpZXcnIHByb3BlcnR5IGlzIG5vdCBzZXQsIHRoZSBiYW5uZXIgaXMgb3ZlcmxheWVkIG9uIHRoZSBjdXJyZW50IHRvcCBtb3N0IHZpZXdcbiAgICAgICAgICAgICAgICAvLyB2aWV3OiAuLixcbiAgICAgICAgICAgICAgICB0ZXN0aW5nOiBmYWxzZSwgLy8gc2V0IHRvIGZhbHNlIHRvIGdldCByZWFsIGJhbm5lcnNcbiAgICAgICAgICAgICAgICBzaXplOiBhZG1vYi5BRF9TSVpFLlNNQVJUX0JBTk5FUiwgLy8gYW55dGhpbmcgaW4gYWRtb2IuQURfU0laRSwgbGlrZSBhZG1vYi5BRF9TSVpFLlNNQVJUX0JBTk5FUlxuICAgICAgICAgICAgICAgIGlvc0Jhbm5lcklkOiBcImNhLWFwcC1wdWItMzk0MDI1NjA5OTk0MjU0NC82MzAwOTc4MTExXCIsIC8vIGFkZCB5b3VyIG93blxuICAgICAgICAgICAgICAgIGFuZHJvaWRCYW5uZXJJZDogXCJjYS1hcHAtcHViLTQ0MjYxNDY0NzA0Nzc4ODcvNzY4MTE5MjIwNFwiLCAvLyBUZXN0IElEXG4gICAgICAgICAgICAgICAgLy8gYW5kcm9pZEJhbm5lcklkOiBcImNhLWFwcC1wdWItNDQyNjE0NjQ3MDQ3Nzg4N345NzYwNTYwNjM0XCIsIC8vIFJlYWwgSURcbiAgICAgICAgICAgICAgICAvLyBBbmRyb2lkIGF1dG9tYXRpY2FsbHkgYWRkcyB0aGUgY29ubmVjdGVkIGRldmljZSBhcyB0ZXN0IGRldmljZSB3aXRoIHRlc3Rpbmc6dHJ1ZSwgaU9TIGRvZXMgbm90XG4gICAgICAgICAgICAgICAgLy8gaW9zVGVzdERldmljZUlkczogW1wieW91clRlc3REZXZpY2VVRElEc1wiLCBcImNhbkJlQWRkZWRIZXJlXCJdLFxuICAgICAgICAgICAgICAgIG1hcmdpbnM6IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgYm90aCBhcmUgc2V0LCB0b3Agd2luc1xuICAgICAgICAgICAgICAgICAgICAvLyB0b3A6IDEwLFxuICAgICAgICAgICAgICAgICAgICBib3R0b206IDBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS50aGVuKFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJhZG1vYiBjcmVhdGVCYW5uZXIgZG9uZVwiKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImFkbW9iIGNyZWF0ZUJhbm5lciBlcnJvcjogXCIgKyBlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSwgMjAwMCk7XG4gICAgfVxuXG4gICAgaGlkZUFkbW9iQmFubmVyKCkge1xuICAgICAgICB2YXIgYWRtb2IgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWFkbW9iXCIpO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vIHRoZSAudGhlbiguLiBiaXQgaXMgb3B0aW9uYWwgYnR3XG4gICAgICAgICAgICBhZG1vYi5oaWRlQmFubmVyKCkudGhlbihcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgaGlkZUJhbm5lciBkb25lXCIpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgaGlkZUJhbm5lciBlcnJvcjogXCIgKyBlcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSwgMzAwMCk7XG5cbiAgICB9XG5cbiAgICBjcmVhdGVBZG1vYkluc2VydGlvbigpIHtcbiAgICAgICAgdmFyIGFkbW9iID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hZG1vYlwiKTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBhZG1vYi5jcmVhdGVJbnRlcnN0aXRpYWwoe1xuICAgICAgICAgICAgICAgIHRlc3Rpbmc6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGlvc0ludGVyc3RpdGlhbElkOiBcImNhLWFwcC1wdWItMzk0MDI1NjA5OTk0MjU0NC82MzAwOTc4MTExXCIsIC8vIGFkZCB5b3VyIG93blxuICAgICAgICAgICAgICAgIGFuZHJvaWRJbnRlcnN0aXRpYWxJZDogXCJjYS1hcHAtcHViLTQ0MjYxNDY0NzA0Nzc4ODcvMjg3NTA4MDkwMFwiLCAvLyBUZXN0IElEXG4gICAgICAgICAgICAgICAgLy8gYW5kcm9pZEludGVyc3RpdGlhbElkOiBcImNhLWFwcC1wdWItNDQyNjE0NjQ3MDQ3Nzg4N345NzYwNTYwNjM0XCIsIC8vIFJlYWwgSURcbiAgICAgICAgICAgICAgICAvLyBBbmRyb2lkIGF1dG9tYXRpY2FsbHkgYWRkcyB0aGUgY29ubmVjdGVkIGRldmljZSBhcyB0ZXN0IGRldmljZSB3aXRoIHRlc3Rpbmc6dHJ1ZSwgaU9TIGRvZXMgbm90XG4gICAgICAgICAgICAgICAgLy8gaW9zVGVzdERldmljZUlkczogW1wiY2U5NzMzMDEzMGM5MDQ3Y2UwZDQ0MzBkMzdkNzEzYjJcIl1cbiAgICAgICAgICAgIH0pLnRoZW4oXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImFkbW9iIGNyZWF0ZUludGVyc3RpdGlhbCBkb25lXCIpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWRtb2IgY3JlYXRlSW50ZXJzdGl0aWFsIGVycm9yOiBcIiArIGVycm9yKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICB9LCAxODAwMDApO1xuICAgIH1cblxuICAgIGZpbmlzaGVkUGxheWluZygpIHtcblxuICAgIH1cblxuICAgIHRhYkNoYW5nZShldmVudCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyhcIlRhYiBcIiArIHRoaXMudGFiU2VsZWN0ZWRJbmRleCArIFwiIFNlbGVjdGVkXCIpO1xuICAgIH1cblxuICAgIGd1aXRhck5vdGVUYXAobm90ZTogc3RyaW5nKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiR3VpdGFyIE5vdGUgVGFwcGVkOiBcIiArIG5vdGUpO1xuICAgICAgICB0aGlzLmFjdGl2ZU5vdGUgPSBub3RlO1xuICAgICAgICB0aGlzLl9wbGF5ZXIucGxheUZyb21GaWxlKHtcbiAgICAgICAgICAgIGF1ZGlvRmlsZTogdGhpcy5ndWl0YXJTb3VuZHNTdGFuZGFyZFtub3RlXSxcbiAgICAgICAgICAgIGxvb3A6IHRoaXMubG9vcFNvdW5kLFxuICAgICAgICAgICAgY29tcGxldGVDYWxsYmFjazogdGhpcy5fdHJhY2tDb21wbGV0ZS5iaW5kKHRoaXMpLFxuICAgICAgICAgICAgZXJyb3JDYWxsYmFjazogdGhpcy5fdHJhY2tFcnJvci5iaW5kKHRoaXMpXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHVrdWxlbGVOb3RlVGFwKG5vdGU6IHN0cmluZykge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkd1aXRhciBOb3RlIFRhcHBlZDogXCIgKyBub3RlKTtcbiAgICAgICAgdGhpcy5hY3RpdmVOb3RlID0gbm90ZTtcbiAgICAgICAgdGhpcy5fcGxheWVyLnBsYXlGcm9tRmlsZSh7XG4gICAgICAgICAgICBhdWRpb0ZpbGU6IHRoaXMudWt1bGVsZVNvdW5kc1N0YW5kYXJkW25vdGVdLFxuICAgICAgICAgICAgbG9vcDogdGhpcy5sb29wU291bmQsXG4gICAgICAgICAgICBjb21wbGV0ZUNhbGxiYWNrOiB0aGlzLl90cmFja0NvbXBsZXRlLmJpbmQodGhpcyksXG4gICAgICAgICAgICBlcnJvckNhbGxiYWNrOiB0aGlzLl90cmFja0Vycm9yLmJpbmQodGhpcylcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYmFzc05vdGVUYXAobm90ZTogc3RyaW5nKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiQmFzcyBOb3RlIFRhcHBlZDogXCIgKyBub3RlKTtcbiAgICAgICAgdGhpcy5hY3RpdmVOb3RlID0gbm90ZTtcbiAgICAgICAgdGhpcy5fcGxheWVyLnBsYXlGcm9tRmlsZSh7XG4gICAgICAgICAgICBhdWRpb0ZpbGU6IHRoaXMuYmFzc1NvdW5kc1N0YW5kYXJkW25vdGVdLFxuICAgICAgICAgICAgbG9vcDogdGhpcy5sb29wU291bmQsXG4gICAgICAgICAgICBjb21wbGV0ZUNhbGxiYWNrOiB0aGlzLl90cmFja0NvbXBsZXRlLmJpbmQodGhpcyksXG4gICAgICAgICAgICBlcnJvckNhbGxiYWNrOiB0aGlzLl90cmFja0Vycm9yLmJpbmQodGhpcylcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc2V0dGluZ3MoKSB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJ3NldHRpbmdzJ10pLCB7XG4gICAgICAgICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJzbGlkZVRvcFwiLFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAyMDAwLFxuICAgICAgICAgICAgICAgIGN1cnZlOiBcImxpbmVhclwiXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcHVibGljIG9uY2hhbmdlKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGBEcm9wIERvd24gc2VsZWN0ZWQgaW5kZXggY2hhbmdlZCBmcm9tICR7YXJncy5vbGRJbmRleH0gdG8gJHthcmdzLm5ld0luZGV4fWApO1xuICAgICAgICBzd2l0Y2ggKGFyZ3MubmV3SW5kZXgpIHtcbiAgICAgICAgICAgIGNhc2UgMDoge1xuICAgICAgICAgICAgICAgIHRoaXMudWt1bGVsZVR1bmluZyA9IFwiU3RhbmRhcmRcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhc2UgMToge1xuICAgICAgICAgICAgICAgIHRoaXMudWt1bGVsZVR1bmluZyA9IFwiU29wcmFub1wiO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2FzZSAyOiB7XG4gICAgICAgICAgICAgICAgdGhpcy51a3VsZWxlVHVuaW5nID0gXCJDb25jZXJ0XCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXNlIDM6IHtcbiAgICAgICAgICAgICAgICB0aGlzLnVrdWxlbGVUdW5pbmcgPSBcIlRlbm9yXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXNlIDQ6IHtcbiAgICAgICAgICAgICAgICB0aGlzLnVrdWxlbGVUdW5pbmcgPSBcIkJhcml0b25lXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkZWZhdWx0OiB7XG4gICAgICAgICAgICAgICAgLy9zdGF0ZW1lbnRzOyBcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBvbm9wZW4oKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRHJvcCBEb3duIG9wZW5lZC5cIik7XG4gICAgfVxuXG4gICAgcHVibGljIG9uY2xvc2UoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRHJvcCBEb3duIGNsb3NlZC5cIik7XG4gICAgfVxuXG4gICAgcHVibGljIGxvb3BDaGVja2VkKGFyZ3MpIHtcbiAgICAgICAgbGV0IGZpcnN0U3dpdGNoID0gPFN3aXRjaD5hcmdzLm9iamVjdDtcbiAgICAgICAgaWYgKGZpcnN0U3dpdGNoLmNoZWNrZWQpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ2hlY2tlZFwiKVxuICAgICAgICAgICAgdGhpcy5sb29wU291bmQgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbi1DaGVja2VkXCIpXG4gICAgICAgICAgICB0aGlzLmxvb3BTb3VuZCA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5hY3RpdmVOb3RlID0gJyc7XG4gICAgICAgICAgICB0aGlzLl9wbGF5ZXIucGF1c2UoKTtcbiAgICAgICAgfVxuICAgIH1cbn0iXX0=