/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.nativescript.tunify;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "org.nativescript.tunify";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "F0F1F2";
  public static final int VERSION_CODE = 6;
  public static final String VERSION_NAME = "1.0";
  public static final String FLAVOR_nativescriptadmob = "F0";
  public static final String FLAVOR_nativescriptaudio = "F1";
  public static final String FLAVOR_nativescriptcardview = "F2";
}
